export default [
    {
        color: "#0000ff",
        width: '3',
        opacity: 0.8,
        code: "orueAqmj_Sx{Eq`@pcB_OjGyBvEyFxBcH\\{IeB}H?eBkFmSk@sFfF_Al@nFgF~@",
        levels: "PAHDCFDBBBEFEP",
        index: '1',
        line: '1'
    },
    {
        color: "#61c982",
        width: '3',
        opacity: 0.8,
        code: "w_veAe`k_SpCa@rCvMpBhALvA_AjAwAJkAm@aEaS",
        levels: "PEDDCGBEP",
        index: '1',
        line: '1'
    },
    {
        color: "#c39205",
        width: '3',
        opacity: 0.8,
        code: "y_veAe`k_S}MrBiNx@{kAgAe[tBwo@lRiYxLan@jo@wMtCwCfDeMzZ_I|VgNzk@oA~Ju@dMmA~Eiq@x|@sC|Bow@xg@{i@i|@eOgJ_Xvp@",
        levels: "PBDDGCIEDGCEDBCGIAHEHP",
        index: '1',
        line: '1'
    },
    {
        color: "#255a88",
        width: '3',
        opacity: 0.8,
        code: "e`geAu~d`SeFfJUrCvD|{@g@rFuqAvxBcCdHuu@pkDe@`EnApt@g@tEad@xvA]|FXzDjAbDjBzD~NzNlKpVtPfn@n@tL{F`t@~@tJ",
        levels: "PCECGCHFCCFICBECEDGDDP",
        index: '1',
        line: '1'
    },
    {
        color: "#87230b",
        width: '3',
        opacity: 0.8,
        code: "ufkeAubn_SbCk@UoBcCb@",
        levels: "PEDP",
        index: '1',
        line: '1'
    },
    {
        color: "#e8eb8e",
        width: '3',
        opacity: 0.8,
        code: "ulueAkrj_Si@oHti@i[hAlCtDH|AgDmBqB_DCsAbC",
        levels: "PFEDEFCDP",
        index: '1',
        line: '1'
    },
    {
        color: "#4ab411",
        width: '3',
        opacity: 0.8,
        code: "i`peAcyl_SzKkBt{@pDbOr@t_@qDxd@_MnnF{nA~[xKtpCsmAeHyMFa@Ec@UWSEm@WmDuE}HnGwStKwmAlh@a@XGf@D|GmQGc@JyUfKqB|Am@lA",
        levels: "PD?EGCGHJC@DA?BGCDFBCFAECBP",
        index: '1',
        line: '1'
    },
    {
        color: "#ac7c94",
        width: '3',
        opacity: 0.8,
        code: "gxseAayk_SzPkAd`ByZH`@zAR|@cAUqAwAUcAbALn@",
        levels: "PDDBECEDCP",
        index: '1',
        line: '1'
    },
    {
        color: "#0e4517",
        width: '3',
        opacity: 0.8,
        code: "ysyeAkak_SPnCtGe@jkAdAxN_AnYkE|]}Qhd@}_@|BfYtCObMyB~AtOaD\\t@pJ~Hy@s@oJ_DX",
        levels: "PECECGDHGBFDEFFEP",
        index: '1',
        line: '1'
    },
    {
        color: "#700d9a",
        width: '3',
        opacity: 0.8,
        code: "osseAqtl_SnBbVhCM",
        levels: "PEP",
        index: '1',
        line: '1'
    },
    {
        color: "#d1d61d",
        width: '3',
        opacity: 0.8,
        code: "gtreAsek_SvGbw@xLvhCd@nAnMpS^lB@rV`CpKsApeCxApK~CxHhqAr`B`ChAtm@xQfb@~\\_A`BdA~BjCGn@uB}@_BeCC}@~AdA`CjCGZ_Afn@de@`KhEjKbCh~BfPMgsA",
        levels: "PDFABFDDECJGBFEDEDCECDECEDIDIP",
        index: '1',
        line: '1'
    },
    {
        color: "#339ea0",
        width: '3',
        opacity: 0.8,
        code: "czfeAwqg~R|AMeH}c@{Hgp@ePivA{CwN{a@o_AcX_`BmAeFw^wv@kAmHmWcsCm@Be@oAj@oArABh@~@_@rAaBFg@oAh@oAd@@`e@so@NqAqRulCsAyTmBgX{AsUgB_WqBwWsCo`@i@kAenCcnC}ZuWue@mo@kKjAkAoMrDc@wAwL}K|BkO`A",
        levels: "PDC?FDFBFGBDCHCDCEDCEBCHA@?A??JBDEFGEDGCP",
        index: '1',
        line: '1'
    },
    {
        color: "#956723",
        width: '3',
        opacity: 0.8,
        code: "wryeAmak_SLbBtF]rkA`AlNiAxK{AvE`VhBbAxBM`AsA`CUk@uJ|i@m[fAjB|F?bCwEuCgEuFCaC|DbAtB",
        levels: "PDCEBGECHCEFEDEFDFDP",
        index: '1',
        line: '1'
    },
    {
        color: "#f72fa6",
        width: '3',
        opacity: 0.8,
        code: "gjreA_ak_S[gE`Gg@[mDwIh@g@iFqFb@",
        levels: "PDEFDEP",
        index: '1',
        line: '1'
    },
    {
        color: "#58f829",
        width: '3',
        opacity: 0.8,
        code: "coueAsxj_SiEjCaDyNiFv@",
        levels: "PEEP",
        index: '1',
        line: '1'
    },
    {
        color: "#bac0ac",
        width: '3',
        opacity: 0.8,
        code: "}dkeAqi__Sl]bBrTiCjl@_ArqAnHf@~_D",
        levels: "PEDEIP",
        index: '1',
        line: '1'
    },
    {
        color: "#1c892f",
        width: '3',
        opacity: 0.8,
        code: "qbleAwsd_SbIfBb_Dq_@|fD_\\lDj@|a@nVtA\\tU|BxBp@jCvAnAjA`A|ArAlA|BrAlAh@~AH~BA|Ai@lB{DdAo@~Bs@pAQfZd@zd@nAfBLlANtAl@hs@dZ",
        levels: "PEDICEABEBA@B@G@BDDB@FA@GB?P",
        index: '1',
        line: '1'
    },
    {
        color: "#7e51b2",
        width: '3',
        opacity: 0.8,
        code: "cjxeAi`d_Sbo@jHxv@yAbFyAa@m\\gGD_EiXhTgLdFiDrHgUxBoHpMp@hJVtNoDxSqA`UoAnGa@vP{@pj@iDhHaJ`MePnIcLnJ_Nr@iBl@gCf@cE@mHKwHUgYAwJS_LEsM?wLM_@cCaDiDyAtAqF|@_AnBsHqIuB_DaAsBeAo@c@",
        levels: "PEDHEEGBF@IAED??@@I@A@EABH@?@AA?FACFBBG@B@P",
        index: '1',
        line: '1'
    },
    {
        color: "#e01a35",
        width: '3',
        opacity: 0.8,
        code: "}qveAcul_Sg\\aQcCc@ii@h@ebDvf@a[jMcdBxj@{KnB_n@`Hao@e@sHkAwwAuk@sp@ddByEjVTbUfDfTzI~SjPzRdRrOd]|Ll_@|L~ZtJfH`FaX|n@",
        levels: "PCFHFCFBHCFKFDIDFCGB@DHP",
        index: '1',
        line: '1'
    },
    {
        color: "#41e2b8",
        width: '3',
        opacity: 0.8,
        code: "mxueAmrj_S@v@Ot@Fn@n@Zz@I^u@~BMI{AcBaAaB`Aa@CUOwDyOePsi@_AXiBsAHiCbCy@`BxAIbC{@\\",
        levels: "PAACEBBFCDC@EBECEDEDCP",
        index: '1',
        line: '1'
    },
    {
        color: "#a3ab3b",
        width: '3',
        opacity: 0.8,
        code: "ys_fA}td_SuFyBy@hP`PbSdvAq~@xuAsj@jDw@`w@}J|Pn@~C`A`DbCpIbMhd@|t@nD`DfDhApFThfBqF|WaKrGbJjHrFbP`EtMy@hMsF`FkFfD{I]qZr@mA~DeDV{Ci@qANaAb@}@dJqAbD_CbD_ElC}FhAaGb@eKtBeJ`DsEpDgCphAae@`HcFrDqGxAqGZgFIkGm@kD}Scw@y@}DiGqRsAqKrFut@k@{KcPan@{KaViN{NeCoFzG@",
        levels: "PFFHGFBIDBFADCHCFGCEFDKCEEBDBBFCDBFDBCDHCFDCIBCEABCGDGEDCFP",
        index: '1',
        line: '1'
    },
    {
        color: "#0573be",
        width: '3',
        opacity: 0.8,
        code: "qwueAogj_ScD{Ag@_DnAcCdDUfCpAb@~DuArB_CNzKxp@yL`h@c@x@{_@lTzLnpGveBaCncArDqBcCmAmCrBw@",
        levels: "PDEDFDDCEGAFGJEFBDP",
        index: '1',
        line: '1'
    },
    {
        color: "#673c41",
        width: '3',
        opacity: 0.8,
        code: "abheAuxm~Rk^wv@C}@hRqCrO@`f@hA`BGbiAgHdz@kJtd@}EznANDe@oCePIk@l@}XJi@vHeK\\WfUiHj@e@pKuYxDqYeBcVAi@hBo_@daArf@`@u@mbAif@}KyBwwF_b@O_sA",
        levels: "PBIDBAED@EIA?DAG@EBFGD?DICKDGIP",
        index: '1',
        line: '1'
    },
    {
        color: "#c904c4",
        width: '3',
        opacity: 0.8,
        code: "mcqeAgk~~Rd@x@pBlC`FeL`GyEvr@mGqQ{iDbZoCb@nCTL~CWVHtApBn@rEVTtnA`Cx@]y@}K^u@fp@qIz@o@~i@}mD?}F_AsEaBsE_C_F{CgCyC_A{YmAi@wAsSyx@WaC}HgTc@oKfF{r@W_McQ{n@oJwVsPaPu@}AdDC",
        levels: "P@EDFHIEDAADBBFGCCFCIJCDACFCDGBCGCDGCEBEP",
        index: '1',
        line: '1'
    },
    {
        color: "#2acd47",
        width: '3',
        opacity: 0.8,
        code: "wykeAmmd_SprBmWdn@eHzkAzrCrv@bfBr@x@be@rYv_@jH~k@^vYzd@b@fAlPbbASz@oOvGa@~@VnQOdBcIdRG~Bz@lBrUhZ",
        levels: "PBJCHBFEGIAGCCFBECGBP",
        index: '1',
        line: '1'
    },
    {
        color: "#8c95ca",
        width: '3',
        opacity: 0.8,
        code: "oiseAsrj_SfGe@lCzg@lA`Z~k@qDbb@gEpKeNSsS~KhBvc@]i@km@gBsRi~B`Rc@{DmQtAZvDqHl@dBlT",
        levels: "PFAHCIEGDHCJEEGEEP",
        index: '1',
        line: '1'
    },
    {
        color: "#ee5e4d",
        width: '3',
        opacity: 0.8,
        code: "{cseAwmk_SY{Ero@mFbr@qM\\vP",
        levels: "PEDGP",
        index: '1',
        line: '1'
    },
    {
        color: "#5026d0",
        width: '3',
        opacity: 0.8,
        code: "wyueA}ej_SdFQtB}C_@eFqD}B}Ef@aCxDh@nDjD`ChHna@dtAqF|v@qHBsKdFPxEeFhb@tHnbC_Bb@h@j@ATi@Uk@e@Ck@l@j@o@",
        levels: "PDFDGDFDDHDFEDGFCBDBDBCP",
        index: '1',
        line: '1'
    },
    {
        color: "#b1ef53",
        width: '3',
        opacity: 0.8,
        code: "sixeAm`d_SnDrwAg{A}D_LfEqg@~cAoK|E{u@tG{`BlQyaBakC}LoHgYlp@",
        levels: "PIHEEJCHEHP",
        index: '1',
        line: '1'
    }
]