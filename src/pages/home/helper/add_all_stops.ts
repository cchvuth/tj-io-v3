import {
  MarkerClusterIcon,
  MarkerClusterOptions,
  GoogleMapsAnimation
} from '@ionic-native/google-maps';
// import bus_stops from '../data/bus-stops';
import DB from '../direction/db';
export async function AddAllStops(map) {
  let markersToBeInserted = [];
  let iconsToBeInserted: MarkerClusterIcon[] = [];
  await DB.forEach(async bus_stop => {
    if (bus_stop == null) return
    await markersToBeInserted.push({
      position: { lat: bus_stop.lat, lng: bus_stop.lng },
      icon: {
        url: 'assets/icon/bus-stop.png',
        size: {
          width: 26,
          height: 34
        },
        anchor: [13, 32]
      },
      info: {
        id: bus_stop.Node_ID,
        line: bus_stop.Line,
        stop_number: bus_stop.Node_ID,
        location: 'st 12',
        summary: 'From Koh Pich to Sen Sok',
        mode: 'walk',
        duration: '5mn'
      },
      visible: true,
      animation: GoogleMapsAnimation.DROP,
      disableAutoPan: false
    })
    await iconsToBeInserted.push({
      url: 'assets/icon/multi-bus-stop.png',
      size: {
        width: 26,
        height: 34
      },
      anchor: [13, 32]
    })
  })

  let options: MarkerClusterOptions = {
    markers: markersToBeInserted,
    icons: iconsToBeInserted,
    maxZoomLevel: 16
  }
  return await map.addMarkerCluster(options)
}
