import {
  Encoding,
  PolylineOptions
} from '@ionic-native/google-maps'

export async function InsertPolyline(map, code, color, width, index){
      let options: PolylineOptions = {
        points: typeof(code) === 'string' ? await Encoding.decodePath(code) : code,
        color: color,
        width: width,
        geodesic: true,
        clickable: true,
        zIndex: index
      }
  return await map.addPolyline(options)
}
