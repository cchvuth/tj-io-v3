import {
    MarkerOptions,
    GoogleMapsAnimation
  } from '@ionic-native/google-maps';
  import { InsertPolyline } from './polyline';

  export async function CreatePath(Map, FullRoute) {
    var createdPoly = [];
    var createdMarker = [];
    for (let i = 0; i < FullRoute.length; i++) { // last index is the same but add it anyway
      for (let j = 0; j < FullRoute[i].length; j++) {
        if (FullRoute[i][j].Path && j != 0) {
          await createdPoly.push(await InsertPolyline(Map, FullRoute[i][j].Path, '#33c15d', 8, 2))
        }
        if (j == 0 || j == FullRoute[i].length - 1) {
          let options: MarkerOptions = {
            icon: {
              url: 'assets/icon/bus-stop.png',
              size: {
                width: 26,
                height: 34
              },
              anchor: [13, 32]
            },
            position: { lat: FullRoute[i][j].lat, lng: FullRoute[i][j].lng },
            visible: true,
            disableAutoPan: false,
            animation: GoogleMapsAnimation.DROP,
            zIndex: 0
          }
          await createdMarker.push(await Map.addMarker(options))
        }
      }
    }
    return {
      createdPoly,
      createdMarker
    }
  }
  