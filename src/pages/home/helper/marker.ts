import {
  MarkerOptions
} from '@ionic-native/google-maps';

export async function AddMarkers(map, name, point, z, width, height) {
  //points.forEach(async point => {
    let options: MarkerOptions = {
      icon: {
        url: `assets/icon/${name}`,
        size: {
          width: width,
          height: height
        },
        anchor: [width/2, name === 'me.gif' ? height/2 : height]
      },
      position: { lat: point.lat, lng: point.lng },
      visible: true,
      zIndex: z
    };
    return map.addMarker(options)
  //});
}
