import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
@Injectable()
export class DataSharingService {

  private tripDetail = new BehaviorSubject(null);
  private message = new BehaviorSubject(null);
  currentTripDetailChanges = this.tripDetail.asObservable();
  currentMessageChanges = this.message.asObservable();

  constructor() { }
  tripDetailChanges(data) {
    data.line = data.line.replace(/[<>]/g, '')
    this.tripDetail.next(data)
  }

  messageChanges(data) {
    this.message.next(data);
  }
}