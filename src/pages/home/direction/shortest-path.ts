import { GeoDif } from './geo-dif'
import DB from './db'
import { InRange } from './in-range'

export async function ShortestPath (User, Destination, bias, precision, NumNodeCloseToUser) {
    if (NumNodeCloseToUser === null) NumNodeCloseToUser = await InRange(User)
    let NumNodeCloseToDestination = [];
    for (let i = 0; i < NumNodeCloseToUser.length; i++) {
        // console.log('Stepping though Line Starting from Node: ', NumNodeCloseToUser[i].Node_ID)
        let CurrentNode = DB[NumNodeCloseToUser[i].Node_ID]
        let CurrentDistanceToDestination = await GeoDif(CurrentNode.lat, CurrentNode.lng, Destination.lat, Destination.lng);
        let NextNode = null, NextNodeDistance = null;

        if (CurrentNode.NextPtr !== null) {
            // console.log('Determining next node distance', JSON.stringify(CurrentNode.NextPtr))
            NextNode = DB[CurrentNode.NextPtr];
            NextNodeDistance = await GeoDif(NextNode.lat, NextNode.lng, Destination.lat, Destination.lng);
        }

        // console.log('Current Distance', CurrentDistanceToDestination, 'Next', NextNodeDistance)
        // Init this index
        NumNodeCloseToDestination[i] = {};

        if (((NextNodeDistance < CurrentDistanceToDestination) || ((NextNodeDistance - CurrentDistanceToDestination) < precision)) && NextNodeDistance != null) {

            // console.log('Next is closer, replacing')
            CurrentDistanceToDestination = NextNodeDistance;
            NumNodeCloseToDestination[i] = NextNode;
            NumNodeCloseToDestination[i].DistanceToDestination = await GeoDif(NextNode.lat, NextNode.lng, Destination.lat, Destination.lng);
            // console.log('Looping forward', JSON.stringify(NextNode))
            while (NextNode.NextPtr != null) {
                // console.log('In while loop moving')
                NextNode = DB[NextNode.NextPtr];
                NextNode.DistanceToDestination = await GeoDif(NextNode.lat, NextNode.lng, Destination.lat, Destination.lng);
                if ((NextNode.DistanceToDestination < CurrentDistanceToDestination) || ((NextNode.DistanceToDestination - CurrentDistanceToDestination) < precision)) {
                    // console.log('Next is closer, replacing')
                    CurrentDistanceToDestination = NextNode.DistanceToDestination;
                    NumNodeCloseToDestination[i] = NextNode;
                } else {
                    // console.log('Next is further, skip');
                    break;
                }
            }
        } else {

            // console.log('Next  is further, skip')
            NumNodeCloseToDestination[i] = NumNodeCloseToUser[i]
            NumNodeCloseToDestination[i].DistanceToDestination = await GeoDif(CurrentNode.lat, CurrentNode.lng, Destination.lat, Destination.lng);
        }
        // console.log('***Line traversed. Node Closest: ', JSON.stringify(NumNodeCloseToDestination[i]))
    }

    // console.log('Finding One Closest to USER AND Destination____________');
    // assuming this is the closest
    let OneClosestDest, OneClosestUser;
    OneClosestDest = {};
    OneClosestDest = NumNodeCloseToDestination[0];
    OneClosestDest.Distance = NumNodeCloseToUser[0].Distance;
    OneClosestUser = NumNodeCloseToUser[0];
    // console.log('Assuming result as', NumNodeCloseToDestination[0].Node_ID, '-', NumNodeCloseToUser[0].Node_ID);
    // console.log('Current Best Route', OneClosestUser.DistanceToDestination + OneClosestUser.Distance)

    for (let i = 1; i < NumNodeCloseToUser.length; i++) {
        // console.log(NumNodeCloseToDestination[i].Node_ID, '-', NumNodeCloseToUser[i].Node_ID)
        // console.log('This Route ', NumNodeCloseToDestination[i].DistanceToDestination, '+', NumNodeCloseToUser[i].Distance, '=', (NumNodeCloseToDestination[i].DistanceToDestination + NumNodeCloseToUser[i].Distance))
        // console.log('Current Best Route', OneClosestUser.DistanceToDestination, '+', OneClosestUser.Distance, '=', OneClosestUser.DistanceToDestination + OneClosestUser.Distance)
        if ((NumNodeCloseToDestination[i].DistanceToDestination + NumNodeCloseToUser[i].Distance * bias) < (OneClosestDest.DistanceToDestination + OneClosestDest.Distance * bias)) {
            // console.log('Replacing: ');
            OneClosestDest = NumNodeCloseToDestination[i];
            OneClosestDest.Distance = NumNodeCloseToUser[i].Distance;
            OneClosestUser = NumNodeCloseToUser[i];
        }
    }


    // console.log('RESULT:', JSON.stringify(OneClosestUser), 'From', OneClosestUser.Node_ID);
    return {
        OneClosestDest: OneClosestDest,
        OneClosestUser: OneClosestUser
    };
}