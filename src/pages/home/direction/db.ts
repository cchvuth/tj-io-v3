export default [
    {
        Node_ID: 0,
        lat: 11.510116,
        lng: 105.052016,
        Path: '',
        NextPtr: 1,
        Line: '1>'
    },
    {
        Node_ID: 1,
        lat: 11.511183,
        lng: 105.048404,
        Path: 'kageA{}d`SkD`HWjAS~@EhAB~ADdA',
        NextPtr: 2,
        Line: '1>'
    },
    {
        Node_ID: 2,
        lat: 11.511229,
        lng: 105.037543,
        Path: '}ggeAihd`SxAjYl@lNNxBL~BAfCYhDUxAs@nB}@~A',
        NextPtr: 3,
        Line: '1>'
    },
    {
        Node_ID: 3,
        lat: 11.515252,
        lng: 105.031585,
        Path: 'uggeAedb`SkBhDsBfDkD|FiBxCaGzJiBtC',
        NextPtr: 4,
        Line: '1>'
    },
    {
        Node_ID: 4,
        lat: 11.52172,
        lng: 105.022224,
        Path: 'ibheAy|``SaHhLaGvJ_CzDsBdDqCrE}AdCkElH',
        NextPtr: 5,
        Line: '1>'
    },
    {
        Node_ID: 5,
        lat: 11.525264,
        lng: 105.015473,
        Path: 'kiieAwc_`S{AdCiBvCkAtBsAzBaBdD}AzDkA`EyAbG',
        NextPtr: 6,
        Line: '1>'
    },
    {
        Node_ID: 6,
        lat: 11.526522,
        lng: 105.011613,
        Path: 'u_jeAaz}_SqBxIkAjF_BbH',
        NextPtr: 7,
        Line: '1>'
    },
    {
        Node_ID: 7,
        lat: 11.529647,
        lng: 105.002195,
        Path: 'shjeAu~|_SaBzGeAxE}@xDy@bDe@pBwAnG}DtP',
        NextPtr: 8,
        Line: '1>'
    },
    {
        Node_ID: 8,
        lat: 11.531179,
        lng: 104.997636,
        Path: 'i{jeAkf{_SkAdF_A|DsAdGoArF',
        NextPtr: 9,
        Line: '1>'
    },
    {
        Node_ID: 9,
        lat: 11.532779,
        lng: 104.991689,
        Path: 'odkeAijz_SkF`Vc@`Ew@vF',
        NextPtr: 10,
        Line: '1>'
    },
    {
        Node_ID: 10,
        lat: 11.533117,
        lng: 104.984414,
        Path: 'ynkeAsdy_SoAdKWlF?jGFtEVrI',
        NextPtr: 11,
        Line: '1>'
    },
    {
        Node_ID: 11,
        lat: 11.536326,
        lng: 104.971892,
        Path: 'aqkeAkww_SVhHJhD?zBKjD[lDcAbFwApEoAxD_ArCoHhUc@lA',
        NextPtr: 12,
        Line: '1>'
    },
    {
        Node_ID: 12,
        lat: 11.536978,
        lng: 104.959843,
        Path: 'celeAiiu_ScCnHu@vBiEdMeAvCq@|Bw@pCUtBItBBvBJtAZhBl@hBd@tAx@pAdAnArCjC~@z@',
        NextPtr: 13,
        Line: '1>'
    },
    {
        Node_ID: 13,
        lat: 11.533374,
        lng: 104.95381,
        Path: '}hleAa~r_S|AbB~@jAhAdBl@nAr@vAx@~AxB`FhBjEdBbF',
        NextPtr: 14,
        Line: '1>'
    },
    {
        Node_ID: 14,
        lat: 11.531038,
        lng: 104.947156,
        Path: 'mrkeAoxq_SzAfFzC~LfEhQLnA',
        NextPtr: 15,
        Line: '1>'
    },
    {
        Node_ID: 15,
        lat: 11.531987,
        lng: 104.936571,
        Path: '}ckeAgnp_SXdEHbE]vF_@zEy@bJy@pJo@lI?tD',
        NextPtr: 16,
        Line: '1>'
    },
    {
        Node_ID: 16,
        lat: 11.530316,
        lng: 104.929672,
        Path: '{ikeAoln_SVhERzA?t@[bAJdAdA~CzB|HnBbJHz@',
        NextPtr: 17,
        Line: '1>'
    },
    {
        Node_ID: 17,
        lat: 11.534295,
        lng: 104.924068,
        Path: 'a_keAkam_S`@nEYjF[bBi@|AgBdDoClCyA~@cBz@{A`@kDb@m@H',
        NextPtr: 18,
        Line: '1>'
    },
    {
        Node_ID: 18,
        lat: 11.534986,
        lng: 104.923864,
        Path: 'kxkeAm}k_S{BP',
        NextPtr: 19,
        Line: '1>'
    },
    {
        Node_ID: 19,
        lat: 11.536157,
        lng: 104.923699,
        Path: 'o|keA{|k_SeF\\',
        NextPtr: 20,
        Line: '1>'
    },
    {
        Node_ID: 20,
        lat: 11.539614,
        lng: 104.923263,
        Path: 'wcleA}{k_SyTlB',
        NextPtr: 21,
        Line: '1>'
    },
    {
        Node_ID: 21,
        lat: 11.548678,
        lng: 104.921782,
        Path: 'syleAoxk_SuZdC}Jx@{Jr@cD^',
        NextPtr: 22,
        Line: '1>'
    },
    {
        Node_ID: 22,
        lat: 11.554405,
        lng: 104.920935,
        Path: 'grneA}ok_SkLbAoKv@mCVkDT',
        NextPtr: 23,
        Line: '1>'
    },
    {
        Node_ID: 23,
        lat: 11.556685,
        lng: 104.920584,
        Path: 'evoeAojk_SqFb@qE^',
        NextPtr: 24,
        Line: '1>'
    },
    {
        Node_ID: 24,
        lat: 11.560427,
        lng: 104.920036,
        Path: 'odpeAkhk_SaFd@sEVkAN}BP}AJ',
        NextPtr: 25,
        Line: '1>'
    },
    {
        Node_ID: 25,
        lat: 11.563541,
        lng: 104.919559,
        Path: 'o{peAcek_SsALaHl@{Fb@',
        NextPtr: 26,
        Line: '1>'
    },
    {
        Node_ID: 26,
        lat: 11.569838,
        lng: 104.918567,
        Path: 'aoqeAcbk_SeWtBmAT}Gf@sBN',
        NextPtr: 27,
        Line: '1>'
    },
    {
        Node_ID: 27,
        lat: 11.572988,
        lng: 104.918068,
        Path: 'kvreA}{j_SyRbB',
        NextPtr: 28,
        Line: '1>'
    },
    {
        Node_ID: 28,
        lat: 11.574686,
        lng: 104.917797,
        Path: 'uiseA{xj_SaJz@',
        NextPtr: 29,
        Line: '1>'
    },
    {
        Node_ID: 29,
        lat: 11.577144,
        lng: 104.917418,
        Path: 'ytseA_wj_S{Fd@kF^',
        NextPtr: 30,
        Line: '1>'
    },
    {
        Node_ID: 30,
        lat: 11.58131,
        lng: 104.916819,
        Path: 'edteAutj_SiIl@cGf@iFd@',
        NextPtr: 31,
        Line: '1>'
    },
    {
        Node_ID: 31,
        lat: 11.607369,
        lng: 104.918316,
        Path: '}}teAypj_S_F`@wHn@i@D}C_Bo@Yy@sD_BmH{PbCoBBuKn@gPSmk@k@_IFyG@yQnA',
        NextPtr: 32,
        Line: '1>'
    },
    {
        Node_ID: 32,
        lat: 11.616581,
        lng: 104.914773,
        Path: 'u`zeAmzj_SeEZcGbBgGjB_Ct@g^`L',
        NextPtr: 33,
        Line: '1>'
    },
    {
        Node_ID: 33,
        lat: 11.626554,
        lng: 104.906672,
        Path: 'oz{eAidj_SyThJcAf@o@f@iBrBwC|CqEpE}QdRoArA',
        NextPtr: 34,
        Line: '1>'
    },
    {
        Node_ID: 34,
        lat: 11.629219,
        lng: 104.904734,
        Path: '_x}eAqqh_SeFlFg@j@gAd@{Ab@cC\\',
        NextPtr: 35,
        Line: '1>'
    },
    {
        Node_ID: 35,
        lat: 11.631906,
        lng: 104.901995,
        Path: 'ui~eAkeh_SwAZeA^o@Zq@l@}@bAi@r@]t@gCjF',
        NextPtr: 36,
        Line: '1>'
    },
    {
        Node_ID: 36,
        lat: 11.634206,
        lng: 104.896887,
        Path: 'az~eAmtg_SsBvEuAfDi@dBq@~BkAlD}AjF',
        NextPtr: 37,
        Line: '1>'
    },
    {
        Node_ID: 37,
        lat: 11.644675,
        lng: 104.875455,
        Path: 'qh_fAotf_SoDbLk@tB}@fDs@bDu@~CeGxW[xBw@|Ki@nFc@bBaCrDeDlEgFbH{K~NgJxL',
        NextPtr: 38,
        Line: '1>'
    },
    {
        Node_ID: 38,
        lat: 11.649949,
        lng: 104.870456,
        Path: 'ojafAemb_S{FvHwBpC}@r@oTfO',
        NextPtr: 39,
        Line: '1>'
    },
    {
        Node_ID: 39,
        lat: 11.654706,
        lng: 104.866894,
        Path: 'mmbfAcma_SiFnDiEtC}LhI',
        NextPtr: 40,
        Line: '1>'
    },
    {
        Node_ID: 40,
        lat: 11.66942,
        lng: 104.86872,
        Path: '_icfAux`_SsHbF{RqZkIeNcEwGuCwD_KsIa@m@]aBgBQu@n@P~A\\f@wVfo@',
        NextPtr: null,
        Line: '1>'
    },
    null,
    null,
    {
        Node_ID: 43,
        lat: 11.6544,
        lng: 104.866817,
        Path: 'ydffA{da_SnVun@hCU~MnMpC~Cz@jAnNbUbLrQhBvC`@h@~DqCdDsB',
        NextPtr: 44,
        Line: '1<'
    },
    {
        Node_ID: 44,
        lat: 11.644114,
        lng: 104.875549,
        Path: 'ggcfAwx`_S~FaE|McJjJoGtKuHlA_AtM{P',
        NextPtr: 45,
        Line: '1<'
    },
    {
        Node_ID: 45,
        lat: 11.637337,
        lng: 104.887242,
        Path: 'agafAapb_Sj^_f@vCaEv@cBt@yCl@kI^aH',
        NextPtr: 47,
        Line: '1<'
    },
    null,
    {
        Node_ID: 47,
        lat: 11.633819,
        lng: 104.897286,
        Path: '}|_fAqxd_SLs@hA_FbAoEzAuGlAgF|AaHx@wDj@mBvDqL',
        NextPtr: 48,
        Line: '1<'
    },
    {
        Node_ID: 48,
        lat: 11.633819,
        lng: 104.897286,
        Path: '{|_fAmxd_SzB}J|GqZ\\kBxCwJrBsG',
        NextPtr: 49,
        Line: '1<'
    },
    {
        Node_ID: 49,
        lat: 11.633819,
        lng: 104.897286,
        Path: 'uf_fAswf_SrEiOzCkH',
        NextPtr: 50,
        Line: '1<'
    },
    {
        Node_ID: 50,
        lat: 11.631698,
        lng: 104.901954,
        Path: 'uf_fAswf_SrEiOzCkHx@cB',
        NextPtr: 51,
        Line: '1<'
    },
    {
        Node_ID: 51,
        lat: 11.628629,
        lng: 104.904754,
        Path: 'ey~eAutg_StByEb@y@d@u@pAwAh@[v@_@vAa@lCc@HC',
        NextPtr: 52,
        Line: '1<'
    },
    {
        Node_ID: 52,
        lat: 11.626582,
        lng: 104.906131,
        Path: '}e~eA}eh_SxA[`Ac@`CgBxBuB',
        NextPtr: 53,
        Line: '1<'
    },
    {
        Node_ID: 53,
        lat: 11.619439,
        lng: 104.913075,
        Path: '_y}eAynh_S`EuE|A{A|IaJrPcQd@a@f@Y',
        NextPtr: 54,
        Line: '1<'
    },
    {
        Node_ID: 54,
        lat: 11.615028,
        lng: 104.915263,
        Path: '_q|eAowi_S`GgCxGoClEkBv@]zE_B',
        NextPtr: 55,
        Line: '1<'
    },
    {
        Node_ID: 55,
        lat: 11.608793,
        lng: 104.917755,
        Path: 'eq{eAmgj_SjVmHxNqE',
        NextPtr: 56,
        Line: '1<'
    },
    {
        Node_ID: 56,
        lat: 11.581612,
        lng: 104.916604,
        Path: 'wizeAowj_SzCq@`G_@hOcAxECzUBtIJjVZlMNxNs@~M{B~DvQd@v@pABfAw@lRcB',
        NextPtr: 57,
        Line: '1<'
    },
    {
        Node_ID: 57,
        lat: 11.577306,
        lng: 104.917262,
        Path: 'e`ueA_pj_S`ZgC',
        NextPtr: 58,
        Line: '1<'
    },
    {
        Node_ID: 58,
        lat: 11.57336,
        lng: 104.917829,
        Path: 'ceteAetj_SxGi@`OoA',
        NextPtr: 59,
        Line: '1<'
    },
    {
        Node_ID: 59,
        lat: 11.570754,
        lng: 104.918126,
        Path: 'alseA}wj_S~NgA',
        NextPtr: 60,
        Line: '1<'
    },
    {
        Node_ID: 60,
        lat: 11.568632,
        lng: 104.91853,
        Path: '}{reA_zj_S`L_A',
        NextPtr: 61,
        Line: '1<'
    },
    {
        Node_ID: 61,
        lat: 11.563252,
        lng: 104.919377,
        Path: '}nreA_|j_SjFs@hCMjCQvBIhE_@nFg@',
        NextPtr: 62,
        Line: '1<'
    },
    {
        Node_ID: 62,
        lat: 11.559121,
        lng: 104.919926,
        Path: 'gmqeAeak_SxX}B',
        NextPtr: 63,
        Line: '1<'
    },
    {
        Node_ID: 63,
        lat: 11.559121,
        lng: 104.919926,
        Path: 'imqeAeak_S|Gq@pBMpCYvGa@',
        NextPtr: 64,
        Line: '1<'
    },
    {
        Node_ID: 64,
        lat: 11.554929,
        lng: 104.92056,
        Path: 'qspeAaek_SfBUfGa@pD]lHi@',
        NextPtr: 65,
        Line: '1<'
    },
    {
        Node_ID: 65,
        lat: 11.552601,
        lng: 104.920948,
        Path: 'yxoeAcik_SjGk@`Jq@zEa@',
        NextPtr: 66,
        Line: '1<'
    },
    {
        Node_ID: 66,
        lat: 11.547481,
        lng: 104.9217,
        Path: '}joeAikk_Sl^wC',
        NextPtr: 67,
        Line: '1<'
    },
    {
        Node_ID: 67,
        lat: 11.543273,
        lng: 104.922296,
        Path: 'okneAapk_SpYcC',
        NextPtr: 68,
        Line: '1<'
    },
    {
        Node_ID: 68,
        lat: 11.538965,
        lng: 104.923018,
        Path: '}pmeActk_SfZ}B',
        NextPtr: 69,
        Line: '1<'
    },
    {
        Node_ID: 69,
        lat: 11.537609,
        lng: 104.923134,
        Path: 'uuleAaxk_SdGg@',
        NextPtr: 70,
        Line: '1<'
    },
    {
        Node_ID: 70,
        lat: 11.536556,
        lng: 104.923337,
        Path: 'gmleAmyk_SrE_@',
        NextPtr: 71,
        Line: '1<'
    },
    {
        Node_ID: 71,
        lat: 11.533737,
        lng: 104.923756,
        Path: 'sfleAmzk_StPuA',
        NextPtr: 72,
        Line: '1<'
    },
    {
        Node_ID: 72,
        lat: 11.530048,
        lng: 104.927031,
        Path: '}tkeAc}k_SnFkAbDoBpCkC|AiCjAuD',
        NextPtr: 73,
        Line: '1<'
    },
    {
        Node_ID: 73,
        lat: 11.530103,
        lng: 104.930233,
        Path: 'm~jeAkql_Sn@{Fy@qJ',
        NextPtr: 74,
        Line: '1<'
    },
    {
        Node_ID: 74,
        lat: 11.531798,
        lng: 104.937076,
        Path: '__keAwdm_SScBDgDyEuOYoAcAqJBwB',
        NextPtr: 75,
        Line: '1<'
    },
    {
        Node_ID: 75,
        lat: 11.53137,
        lng: 104.940288,
        Path: 'aikeAson_SVmF~@oK',
        NextPtr: 76,
        Line: '1<'
    },
    {
        Node_ID: 76,
        lat: 11.533508,
        lng: 104.954668,
        Path: 'afkeAyco_ShC}ZGsH{@uGwN{j@',
        NextPtr: 77,
        Line: '1<'
    },
    {
        Node_ID: 77,
        lat: 11.536474,
        lng: 104.959664,
        Path: 'sskeA{}q_SuFoMwDcIwGaI',
        NextPtr: null,
        Line: '1<'
    },
    {
        Node_ID: 78,
        lat: 11.58409,
        lng: 104.91745,
        Path: '',
        NextPtr: 79,
        Line: '2>'
    },
    {
        Node_ID: 79,
        lat: 11.5798162,
        lng: 104.920546,
        Path: 'youeAeuj_SEe@vLcHtKoG',
        NextPtr: 80,
        Line: '2>'
    },
    {
        Node_ID: 80,
        lat: 11.577793,
        lng: 104.9218265,
        Path: 'outeA}gk_SrGyDnAy@r@[',
        NextPtr: 81,
        Line: '2>'
    },
    {
        Node_ID: 81,
        lat: 11.5717503,
        lng: 104.9237838,
        Path: 'uhteAmpk_S`CuAzAEhAp@~AChASnAsAP}Bd@AtBSxAI~C[|C[fBW',
        NextPtr: 82,
        Line: '2>'
    },
    {
        Node_ID: 82,
        lat: 11.5658137,
        lng: 104.9254277,
        Path: 'gbseAo|k_SrNsChH{AzGeAbB[',
        NextPtr: 83,
        Line: '2>'
    },
    {
        Node_ID: 83,
        lat: 11.5644725,
        lng: 104.9258444,
        Path: 'm}qeAagl_SpGoA',
        NextPtr: 84,
        Line: '2>'
    },
    {
        Node_ID: 84,
        lat: 11.5623775,
        lng: 104.9264627,
        Path: 'wtqeAmil_S`E_AvEw@',
        NextPtr: 85,
        Line: '2>'
    },
    {
        Node_ID: 85,
        lat: 11.5529387,
        lng: 104.9287429,
        Path: '_hqeAeml_SfDs@`Dm@nFeAjF_AxFkAj@Ch@b@p@Rp@Y`@k@d@Y|Ao@ZOxCk@p@Qb@CpER',
        NextPtr: 86,
        Line: '2>'
    },
    {
        Node_ID: 86,
        lat: 11.5503673,
        lng: 104.9285166,
        Path: 'gmoeAs{l_SjDHtCRfEN',
        NextPtr: 87,
        Line: '2>'
    },
    {
        Node_ID: 87,
        lat: 11.5459567,
        lng: 104.9280653,
        Path: '}|neAezl_StCNdBF`DL|CN|Hb@',
        NextPtr: 88,
        Line: '2>'
    },
    {
        Node_ID: 88,
        lat: 11.545131,
        lng: 104.927924,
        Path: 'ganeAmwl_SbDJ',
        NextPtr: 89,
        Line: '2>'
    },
    {
        Node_ID: 89,
        lat: 11.5364218,
        lng: 104.928647,
        Path: '_|meA_wl_SvCJtBJtBFrAJ`DHv@A~Hq@nFk@~KiApAO',
        NextPtr: 90,
        Line: '2>'
    },
    {
        Node_ID: 90,
        lat: 11.5327645,
        lng: 104.9299489,
        Path: 'seleAa{l_SrUqG',
        NextPtr: 91,
        Line: '2>'
    },
    {
        Node_ID: 91,
        lat: 11.528661,
        lng: 104.931489,
        Path: 'eokeAmcm_SxSsFdDy@',
        NextPtr: 92,
        Line: '2>'
    },
    {
        Node_ID: 92,
        lat: 11.523035,
        lng: 104.932495,
        Path: 'aujeA{lm_SrCy@jDUdCKpDU`Ga@jDa@f@O',
        NextPtr: 93,
        Line: '2>'
    },
    {
        Node_ID: 93,
        lat: 11.521309,
        lng: 104.933132,
        Path: 'urieA}rm_SdJgC',
        NextPtr: 94,
        Line: '2>'
    },
    {
        Node_ID: 94,
        lat: 11.517227,
        lng: 104.93476,
        Path: 'ogieAewm_SfPwEtGiB',
        NextPtr: 95,
        Line: '2>'
    },
    {
        Node_ID: 95,
        lat: 11.51251,
        lng: 104.935822,
        Path: 'gmheAgan_S|DiAt@QlAOdDOnGa@v@Ov@[b@M',
        NextPtr: 96,
        Line: '2>'
    },
    {
        Node_ID: 96,
        lat: 11.505242,
        lng: 104.939079,
        Path: 'ipgeAghn_SlWgK`EsAxFaBhCg@',
        NextPtr: 97,
        Line: '2>'
    },
    {
        Node_ID: 97,
        lat: 11.501423,
        lng: 104.940422,
        Path: 'uefeAm{n_SbNoCtJ}C',
        NextPtr: 98,
        Line: '2>'
    },
    {
        Node_ID: 98,
        lat: 11.497695,
        lng: 104.941969,
        Path: 'sjeeAwdo_SpJaDdBi@hGmB',
        NextPtr: 99,
        Line: '2>'
    },
    {
        Node_ID: 99,
        lat: 11.494517,
        lng: 104.942983,
        Path: 'gsdeAwno_S`Cw@`A[rAWvIwA',
        NextPtr: 100,
        Line: '2>'
    },
    {
        Node_ID: 100,
        lat: 11.491862,
        lng: 104.943397,
        Path: 'y_deAyto_SvHuA`BKpA@h@F',
        NextPtr: 101,
        Line: '2>'
    },
    {
        Node_ID: 101,
        lat: 11.48844,
        lng: 104.941892,
        Path: 'coceAqwo_SfTpH',
        NextPtr: 102,
        Line: '2>'
    },
    {
        Node_ID: 102,
        lat: 11.481766,
        lng: 104.944576,
        Path: 'yybeA{mo_SjCz@~AIda@iQ',
        NextPtr: 103,
        Line: '2>'
    },
    {
        Node_ID: 103,
        lat: 11.480533,
        lng: 104.945271,
        Path: 'cpaeAu~o_Sj@Yn@DLi@pCgA',
        NextPtr: 104,
        Line: '2>'
    },
    {
        Node_ID: 104,
        lat: 11.479137,
        lng: 104.94598,
        Path: '_haeA_cp_ShGsC',
        NextPtr: 105,
        Line: '2>'
    },
    {
        Node_ID: 105,
        lat: 11.475816,
        lng: 104.947818,
        Path: 's_aeAkgp_SrSmJ',
        NextPtr: 106,
        Line: '2>'
    },
    {
        Node_ID: 106,
        lat: 11.473104,
        lng: 104.949255,
        Path: '{j`eAwrp_ShD{AhCkAdFsB',
        NextPtr: 107,
        Line: '2>'
    },
    {
        Node_ID: 107,
        lat: 11.46977,
        lng: 104.951027,
        Path: 'az_eAu{p_SjD}AtCkAtFcChBy@',
        NextPtr: 108,
        Line: '2>'
    },
    {
        Node_ID: 108,
        lat: 11.46695,
        lng: 104.95254,
        Path: 'ge_eAwfq_StHmDzFaC',
        NextPtr: 109,
        Line: '2>'
    },
    {
        Node_ID: 109,
        lat: 11.464884,
        lng: 104.953616,
        Path: 'ss~dAepq_SzKeF',
        NextPtr: 110,
        Line: '2>'
    },
    {
        Node_ID: 110,
        lat: 11.467861,
        lng: 104.957467,
        Path: 'sf~dAkwq_ShD{AeH}LKcAc@_Aa@OeEcF{DxC',
        NextPtr: null,
        Line: '2>'
    },
    {
        Node_ID: 111,
        lat: 11.470571,
        lng: 104.955641,
        Path: '{x~dAgor_SiC|BcAl@}FpC{At@',
        NextPtr: 112,
        Line: '2<'
    },
    {
        Node_ID: 112,
        lat: 11.474569,
        lng: 104.953461,
        Path: '_j_eAucr_SmIfE_GfCkC`AcA^',
        NextPtr: 113,
        Line: '2<'
    },
    {
        Node_ID: 113,
        lat: 11.478991,
        lng: 104.951053,
        Path: 'wb`eAcvq_SwChAiBbAwRvI',
        NextPtr: 114,
        Line: '2<'
    },
    {
        Node_ID: 114,
        lat: 11.484238,
        lng: 104.948363,
        Path: 'w~`eA}fq_Sg`@hP',
        NextPtr: 115,
        Line: '2<'
    },
    {
        Node_ID: 115,
        lat: 11.484671,
        lng: 104.946831,
        Path: '}_beAsup_Sy@d@EbABzD',
        NextPtr: 116,
        Line: '2<'
    },
    {
        Node_ID: 116,
        lat: 11.487235,
        lng: 104.94652,
        Path: 'yabeAilp_S?|@wFEuEC}@A',
        NextPtr: 117,
        Line: '2<'
    },
    {
        Node_ID: 117,
        lat: 11.492823,
        lng: 104.943626,
        Path: 'arbeAwjp_SkA@wHxCsFbC}BhAmBfAuB`BoA^w@N',
        NextPtr: 118,
        Line: '2<'
    },
    {
        Node_ID: 118,
        lat: 11.495315,
        lng: 104.942985,
        Path: 'cuceAsxo_SsN`C',
        NextPtr: 119,
        Line: '2<'
    },
    {
        Node_ID: 119,
        lat: 11.498905,
        lng: 104.941668,
        Path: '}ddeAoto_S{Ev@wA^uAd@_IfC',
        NextPtr: 120,
        Line: '2<'
    },
    {
        Node_ID: 120,
        lat: 11.498905,
        lng: 104.941668,
        Path: '}ddeAoto_S{Ev@wA^uAd@_IfC',
        NextPtr: 121,
        Line: '2<'
    },
    {
        Node_ID: 121,
        lat: 11.502758,
        lng: 104.940059,
        Path: 'e{deAilo_SsJxCkKdD',
        NextPtr: 122,
        Line: '2<'
    },
    {
        Node_ID: 122,
        lat: 11.505853,
        lng: 104.939077,
        Path: 'iseeAgbo_S_D`AiCh@{HrA',
        NextPtr: 123,
        Line: '2<'
    },
    {
        Node_ID: 123,
        lat: 11.509194,
        lng: 104.937773,
        Path: 'mffeAg|n_S}G`BaCr@uBt@aC`A',
        NextPtr: 124,
        Line: '2<'
    },
    {
        Node_ID: 124,
        lat: 11.512966,
        lng: 104.935926,
        Path: 'e{feA{sn_SqFxBcO~F',
        NextPtr: 125,
        Line: '2<'
    },
    {
        Node_ID: 125,
        lat: 11.517251,
        lng: 104.934907,
        Path: 'yrgeAchn_S}Ad@_Mr@uANsFvA',
        NextPtr: 126,
        Line: '2<'
    },
    {
        Node_ID: 126,
        lat: 11.520231,
        lng: 104.933789,
        Path: '}mheAabn_SgL~CaElA',
        NextPtr: 127,
        Line: '2<'
    },
    {
        Node_ID: 127,
        lat: 11.522723,
        lng: 104.932823,
        Path: 'gaieAuzm_S{K~CaAT',
        NextPtr: 128,
        Line: '2<'
    },
    {
        Node_ID: 128,
        lat: 11.52784,
        lng: 104.931954,
        Path: 'cpieA_um_SaD~@{YjB',
        NextPtr: 129,
        Line: '2<'
    },
    {
        Node_ID: 129,
        lat: 11.5328233,
        lng: 104.9301668,
        Path: '}ojeAsom_SqCn@kSrFgEnA',
        NextPtr: 130,
        Line: '2<'
    },
    {
        Node_ID: 130,
        lat: 11.5371904,
        lng: 104.9286503,
        Path: '}nkeAadm_SwJjCaKlCqBP',
        NextPtr: 131,
        Line: '2<'
    },
    {
        Node_ID: 131,
        lat: 11.5428988,
        lng: 104.9279064,
        Path: 'ajleAuzl_Sy[|CaA?eDQ',
        NextPtr: 132,
        Line: '2<'
    },
    {
        Node_ID: 132,
        lat: 11.5460326,
        lng: 104.9281907,
        Path: '}mmeAevl_ScH[qIY',
        NextPtr: 133,
        Line: '2<'
    },
    {
        Node_ID: 133,
        lat: 11.5499761,
        lng: 104.9286021,
        Path: 'kaneA}wl_SgMo@uI_@',
        NextPtr: 134,
        Line: '2<'
    },
    {
        Node_ID: 134,
        lat: 11.5536666,
        lng: 104.9289068,
        Path: 'kzneAmzl_SiHUkH]iCK',
        NextPtr: 135,
        Line: '2<'
    },
    {
        Node_ID: 135,
        lat: 11.558375,
        lng: 104.927751,
        Path: 'oqoeAk|l_SiAEuFbAoCDaA]wA^U~@}@`@iCb@',
        NextPtr: 136,
        Line: '2<'
    },
    {
        Node_ID: 136,
        lat: 11.5621795,
        lng: 104.9266635,
        Path: 'wkpeA{ul_ScZvF',
        NextPtr: 137,
        Line: '2<'
    },
    {
        Node_ID: 137,
        lat: 11.5668907,
        lng: 104.925368,
        Path: 'sfqeAgnl_SsV|EgDh@',
        NextPtr: 138,
        Line: '2<'
    },
    {
        Node_ID: 138,
        lat: 11.568352,
        lng: 104.9249157,
        Path: 'ucreA{el_SqHpA',
        NextPtr: 139,
        Line: '2<'
    },
    {
        Node_ID: 139,
        lat: 11.57176,
        lng: 104.92387,
        Path: 'cmreAocl_SgDt@}NrC',
        NextPtr: 140,
        Line: '2<'
    },
    {
        Node_ID: 140,
        lat: 11.57803,
        lng: 104.92188,
        Path: 'qbseAc}k_SaCb@wNlA_@[k@uA}CcAwBl@oArDYjCcDnB',
        NextPtr: 141,
        Line: '2<'
    },
    {
        Node_ID: 141,
        lat: 11.58036,
        lng: 104.92036,
        Path: 'uiteAkpk_S{MvH',
        NextPtr: null,
        Line: '2<'
    },
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    {
        Node_ID: 200,
        lat: 11.50214,
        lng: 104.83232,
        Path: '',
        NextPtr: 201,
        Line: '3>'
    },
    {
        Node_ID: 201,
        lat: 11.50203,
        lng: 104.81915,
        Path: 'koeeA_az~RThqA',
        NextPtr: 202,
        Line: '3>'
    },
    {
        Node_ID: 202,
        lat: 11.50403,
        lng: 104.8191,
        Path: 'uneeAunw~RA~@mKu@',
        NextPtr: 203,
        Line: '3>'
    },
    {
        Node_ID: 203,
        lat: 11.51243,
        lng: 104.82024,
        Path: 'e{eeAknw~Ros@cF',
        NextPtr: 204,
        Line: '3>'
    },
    {
        Node_ID: 204,
        lat: 11.51767,
        lng: 104.82095,
        Path: 'uogeAouw~Rw_@mC',
        NextPtr: 205,
        Line: '3>'
    },
    {
        Node_ID: 205,
        lat: 11.52365,
        lng: 104.82195,
        Path: 'mpheA}yw~R}YsBkBUaDg@_AU',
        NextPtr: 206,
        Line: '3>'
    },
    {
        Node_ID: 206,
        lat: 11.52619,
        lng: 104.82315,
        Path: 'yuieAe`x~R}C{@yBy@cFyB',
        NextPtr: 207,
        Line: '3>'
    },
    {
        Node_ID: 207,
        lat: 11.53276,
        lng: 104.82854,
        Path: 'uejeAugx~Re@WmDyBeIqGwCaCyJ}HuFqE',
        NextPtr: 208,
        Line: '3>'
    },
    {
        Node_ID: 208,
        lat: 11.54149,
        lng: 104.83539,
        Path: 'wnkeAkiy~RsDuCQQ_@Wg@WCG}@_Ae@S{A{A{@q@w@u@uC_CkSaPyA_Aw@a@_Cw@wAe@',
        NextPtr: 209,
        Line: '3>'
    },
    {
        Node_ID: 209,
        lat: 11.54372,
        lng: 104.83627,
        Path: 'iemeAetz~R}LoD',
        NextPtr: 210,
        Line: '3>'
    },
    {
        Node_ID: 210,
        lat: 11.54688,
        lng: 104.83754,
        Path: 'gsmeAuyz~RoBm@gOoE',
        NextPtr: 211,
        Line: '3>'
    },
    {
        Node_ID: 211,
        lat: 11.54993,
        lng: 104.83995,
        Path: '_gneAsa{~R_FaBkBkAgA_AmEsF',
        NextPtr: 212,
        Line: '3>'
    },
    {
        Node_ID: 212,
        lat: 11.55358,
        lng: 104.84422,
        Path: 'azneAup{~RoDkEgB{CUW{@cAsCcD]WuAeBc@k@WQk@q@',
        NextPtr: 213,
        Line: '3>'
    },
    {
        Node_ID: 213,
        lat: 11.56022,
        lng: 104.852,
        Path: '{poeAkk|~RsAaBuBkCGUyE}FSWgAwA_LcN{@gAkIuJ',
        NextPtr: 214,
        Line: '3>'
    },
    {
        Node_ID: 214,
        lat: 11.56231,
        lng: 104.85532,
        Path: 'kzpeA_|}~R]a@sCyDqBqD}BiG',
        NextPtr: 215,
        Line: '3>'
    },
    {
        Node_ID: 215,
        lat: 11.56307,
        lng: 104.85891,
        Path: 'mgqeAwp~~R[}@yA{Ha@sI',
        NextPtr: 216,
        Line: '3>'
    },
    {
        Node_ID: 216,
        lat: 11.56297,
        lng: 104.86278,
        Path: 'elqeAeg__SCmAVwT',
        NextPtr: 217,
        Line: '3>'
    },
    {
        Node_ID: 217,
        lat: 11.56291,
        lng: 104.86483,
        Path: 'qkqeAk_`_SJyK',
        NextPtr: 218,
        Line: '3>'
    },
    {
        Node_ID: 218,
        lat: 11.56262,
        lng: 104.87565,
        Path: 'ekqeAel`_S?mFPaHNgMBsELmHBuDEcDF_I',
        NextPtr: 219,
        Line: '3>'
    },
    {
        Node_ID: 219,
        lat: 11.563104137444135,
        lng: 104.88136558618999,
        Path: 'kiqeAyob_S@aA?sAByC@g@@s@I_HGeAs@mEYaBGk@Ai@',
        NextPtr: 220,
        Line: '3>'
    },
    {
        Node_ID: 220,
        lat: 11.564,
        lng: 104.88638,
        Path: 'klqeAqsc_SCcA?oBQ}MAc@Go@Kk@Sq@sBeD',
        NextPtr: 221,
        Line: '3>'
    },
    {
        Node_ID: 221,
        lat: 11.56619,
        lng: 104.89232,
        Path: '_rqeA{rd_SKOMKyFuIa@g@g@u@I[GWOcC[aGYyG',
        NextPtr: 222,
        Line: '3>'
    },
    {
        Node_ID: 222,
        lat: 11.56688,
        lng: 104.899,
        Path: 'u_reA_xe_SWoF}@oQe@eIMqD',
        NextPtr: 223,
        Line: '3>'
    },
    {
        Node_ID: 223,
        lat: 11.56732,
        lng: 104.90297,
        Path: '_dreAwag_Sk@oKk@iK',
        NextPtr: 224,
        Line: '3>'
    },
    {
        Node_ID: 224,
        lat: 11.56756,
        lng: 104.90532,
        Path: '_dreAwag_Sk@oKk@iKo@uM',
        NextPtr: 225,
        Line: '3>'
    },
    {
        Node_ID: 225,
        lat: 11.56806,
        lng: 104.91033,
        Path: 'ghreAgih_SScEIaAA]cAeU',
        NextPtr: 226,
        Line: '3>'
    },
    {
        Node_ID: 226,
        lat: 11.5686,
        lng: 104.91452,
        Path: 'kkreAqhi_SIuBIeCwAiQ',
        NextPtr: 227,
        Line: '3>'
    },
    {
        Node_ID: 227,
        lat: 11.57341,
        lng: 104.92509,
        Path: 'wnreAwbj_SyAcSKuAGcACYKWWgCGiAMqAlBSFAHEBO?IOgCG{@S{B_CRsAiHg@sCOWCVcLtBoCRi@wH',
        NextPtr: 228,
        Line: '3>'
    },
    {
        Node_ID: 228,
        lat: 11.57723,
        lng: 104.92529,
        Path: 'wnreAwbj_SyAcSKuAGcACYKWWgCGiAMqAlBSFAHEBO?IOgCG{@S{B_CRsAiHg@sCOWCVcLtBoCRi@wHq@gIo@gGwAhAqEbDoJxH',
        NextPtr: 229,
        Line: '3>'
    },
    {
        Node_ID: 229,
        lat: 11.57983,
        lng: 104.92306,
        Path: 'udteAafl_SgO|L',
        NextPtr: 230,
        Line: '3>'
    },
    {
        Node_ID: 230,
        lat: 11.58318,
        lng: 104.92084,
        Path: '}tteAcxk_SuC|BgO|H',
        NextPtr: 231,
        Line: '3>'
    },
    {
        Node_ID: 231,
        lat: 11.6052,
        lng: 104.91898,
        Path: '{iueAgjk_SqKpF{A^cS~CYCsNt@_A?ah@q@ia@I}F\\IqA',
        NextPtr: null,
        Line: '3>'
    },
    {
        Node_ID: 232,
        lat: 11.6052,
        lng: 104.91898,
        Path: '',
        NextPtr: 233,
        Line: '3<'
    },
    {
        Node_ID: 233,
        lat: 11.58256,
        lng: 104.92119,
        Path: 'osyeAs~j_SHrAiObA@PdHe@~F_@hF]d]@vLPlJLdINpJHtNu@VKfS}CvA_@fMqGbAi@',
        NextPtr: 234,
        Line: '3<'
    },
    {
        Node_ID: 234,
        lat: 11.57943,
        lng: 104.92336,
        Path: '_fueAmlk_SlKuFbF{D',
        NextPtr: 235,
        Line: '3<'
    },
    {
        Node_ID: 235,
        lat: 11.57401,
        lng: 104.92483,
        Path: 'mrteA_zk_Sh^kYpAdP',
        NextPtr: 236,
        Line: '3<'
    },
    {
        Node_ID: 236,
        lat: 11.56817,
        lng: 104.91071,
        Path: 'qpseAecl_Sh@|GfGi@dLwBrB`MqBRAVj@nHBHLBtBOR~CTdCC\\jAfOLb@bC~^',
        NextPtr: 237,
        Line: '3<'
    },
    {
        Node_ID: 237,
        lat: 11.56766,
        lng: 104.90523,
        Path: 'alreA}ji_SdBfa@',
        NextPtr: 238,
        Line: '3<'
    },
    {
        Node_ID: 238,
        lat: 11.56741,
        lng: 104.9028,
        Path: '{hreAuhh_Sp@dN',
        NextPtr: 239,
        Line: '3<'
    },
    {
        Node_ID: 239,
        lat: 11.56698,
        lng: 104.89891,
        Path: 'igreAoyg_StAhW',
        NextPtr: 240,
        Line: '3<'
    },
    {
        Node_ID: 240,
        lat: 11.56628,
        lng: 104.89224,
        Path: 'sdreAeag_SjCth@',
        NextPtr: 241,
        Line: '3<'
    },
    {
        Node_ID: 241,
        lat: 11.56397,
        lng: 104.88607,
        Path: 'g`reAowe_Sz@`QGh@Sf@hG`JTHlCpE',
        NextPtr: 242,
        Line: '3<'
    },
    {
        Node_ID: 242,
        lat: 11.56306,
        lng: 104.88066,
        Path: 'yqqeA}pd_S|@rAd@nARrAXvNHxB@bAVjC',
        NextPtr: 243,
        Line: '3<'
    },
    {
        Node_ID: 243,
        lat: 11.5627,
        lng: 104.87668,
        Path: 'clqeAcoc_S~@fGFrO',
        NextPtr: 244,
        Line: '3<'
    },
    {
        Node_ID: 244,
        lat: 11.56305,
        lng: 104.86616,
        Path: '{iqeAgvb_SChCi@n`@UvRAdG',
        NextPtr: 245,
        Line: '3<'
    },
    {
        Node_ID: 245,
        lat: 11.56312,
        lng: 104.86245,
        Path: 'alqeAot`_S@fFO|N',
        NextPtr: 246,
        Line: '3<'
    },
    {
        Node_ID: 246,
        lat: 11.56319,
        lng: 104.85891,
        Path: 'olqeAi}__SMbU',
        NextPtr: 247,
        Line: '3<'
    },
    {
        Node_ID: 247,
        lat: 11.56247,
        lng: 104.85533,
        Path: '}lqeAeg__S@~@RlEJhAlBrJ',
        NextPtr: 248,
        Line: '3<'
    },
    {
        Node_ID: 248,
        lat: 11.53296,
        lng: 104.82866,
        Path: 'mhqeAyp~~R|EdL`EtFjGbHvW~[rA|A~FjHPb@tBhCTLpB`C`@h@hEzFjBzB|GjIPPvBxARNdA`@nC|@zDfAn\\|J|Bp@r@Vv@^tBtAxXdU`C`En@bBh@Ln@GxAInCtB',
        NextPtr: 249,
        Line: '3<'
    },
    {
        Node_ID: 249,
        lat: 11.52624,
        lng: 104.82311,
        Path: '_pkeAcjy~RvGrFvYnUzBvArAx@',
        NextPtr: 250,
        Line: '3<'
    },
    {
        Node_ID: 250,
        lat: 11.52356,
        lng: 104.82184,
        Path: '_fjeAmgx~RhFzBrBv@xDhA',
        NextPtr: 251,
        Line: '3<'
    },
    {
        Node_ID: 251,
        lat: 11.51808,
        lng: 104.82095,
        Path: 'auieAq_x~Rl@L`Dh@pZzB',
        NextPtr: 252,
        Line: '3<'
    },
    {
        Node_ID: 252,
        lat: 11.51104,
        lng: 104.81999,
        Path: '_sheA}yw~RhAFlBPtF^nDXdBLzT|A',
        NextPtr: 253,
        Line: '3<'
    },
    {
        Node_ID: 253,
        lat: 11.50393,
        lng: 104.81904,
        Path: '_ggeA}sw~RjBNrE\\|F\\nYpB',
        NextPtr: 254,
        Line: '3<'
    },
    {
        Node_ID: 254,
        lat: 11.50211,
        lng: 104.83231,
        Path: 'qzeeA_nw~RbKr@WqsA',
        NextPtr: null,
        Line: '3<'
    },
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    {
        Node_ID: 400,
        lat: 11.54479,
        lng: 104.933003,
        Path: '',
        NextPtr: 401,
        Line: '5>'
    },
    {
        Node_ID: 401,
        lat: 11.545572,
        lng: 104.930294,
        Path: 'wymeAevm_Se@nB[n@UXc@ZaBjGENbAZ',
        NextPtr: 402,
        Line: '5>'
    },
    {
        Node_ID: 402,
        lat: 11.544315,
        lng: 104.92396,
        Path: 'w~meAuem_SbAZd@RPNVXt@|@^h@NXFPFb@?p@KrCBvGDzIDnD',
        NextPtr: 403,
        Line: '5>'
    },
    {
        Node_ID: 403,
        lat: 11.544277,
        lng: 104.921837,
        Path: 'ivmeAy}k_SD|GAtC',
        NextPtr: 404,
        Line: '5>'
    },
    {
        Node_ID: 404,
        lat: 11.544066,
        lng: 104.915866,
        Path: 'cvmeA{pk_S`@ve@',
        NextPtr: 405,
        Line: '5>'
    },
    {
        Node_ID: 405,
        lat: 11.544883,
        lng: 104.911202,
        Path: 'aumeAcjj_SHlIExCIfA]tBk@fBm@vA_@j@',
        NextPtr: 406,
        Line: '5>'
    },
    {
        Node_ID: 406,
        lat: 11.547185,
        lng: 104.908184,
        Path: '{ymeAwmi_SyAzBqAbBaBxBmE|F',
        NextPtr: 407,
        Line: '5>'
    },
    {
        Node_ID: 407,
        lat: 11.549988,
        lng: 104.904842,
        Path: 'whneA_{h_S_DhEgGhIiAxAs@~@',
        NextPtr: 408,
        Line: '5>'
    },
    {
        Node_ID: 408,
        lat: 11.553481,
        lng: 104.902118,
        Path: '}yneAqeh_SqBlCiGjIKJuAPiBLaCJ',
        NextPtr: 409,
        Line: '5>'
    },
    {
        Node_ID: 409,
        lat: 11.560486,
        lng: 104.901279,
        Path: 'gpoeA_ug_SyGb@kDVwKl@gKl@mCP',
        NextPtr: 410,
        Line: '5>'
    },
    {
        Node_ID: 410,
        lat: 11.566219,
        lng: 104.900627,
        Path: '{{peAuog_S{@FsDTsDNsCLoDV_ABaCNuEV',
        NextPtr: 411,
        Line: '5>'
    },
    {
        Node_ID: 411,
        lat: 11.571447,
        lng: 104.899739,
        Path: '{_reAukg_SuDPyF^eCNy@@y@By@D}GfB',
        NextPtr: 412,
        Line: '5>'
    },
    {
        Node_ID: 412,
        lat: 11.576824,
        lng: 104.899007,
        Path: '_`seA}eg_SyBp@cBb@]D}@?mEOkHa@cFW?Ve@|A',
        NextPtr: 413,
        Line: '5>'
    },
    {
        Node_ID: 413,
        lat: 11.579317,
        lng: 104.894112,
        Path: '}ateAuag_SiEfNeBnFoApDQPm@^s@Z',
        NextPtr: 414,
        Line: '5>'
    },
    {
        Node_ID: 414,
        lat: 11.583245,
        lng: 104.890788,
        Path: 'qqteA}bf_Ss@ZwHfEwG|DaDfBC?`@bC',
        NextPtr: 415,
        Line: '5>'
    },
    {
        Node_ID: 415,
        lat: 11.582574,
        lng: 104.888366,
        Path: 'wiueAcne_S|BvM',
        NextPtr: 416,
        Line: '5>'
    },
    {
        Node_ID: 416,
        lat: 11.581013,
        lng: 104.886404,
        Path: 'yeueAk_e_Sh@|CCd@zA?pDMDtE',
        NextPtr: 417,
        Line: '5>'
    },
    {
        Node_ID: 417,
        lat: 11.580917,
        lng: 104.883006,
        Path: '_|teA_sd_S@lGTxK',
        NextPtr: 418,
        Line: '5>'
    },
    {
        Node_ID: 418,
        lat: 11.584629,
        lng: 104.882275,
        Path: 'g{teAw}c_SDn@_FpAg@FqNZ',
        NextPtr: 419,
        Line: '5>'
    },
    {
        Node_ID: 419,
        lat: 11.590892,
        lng: 104.881961,
        Path: '_sueAqxc_SkCDeADuHRuELoEHu@?iBB?OwBH[?',
        NextPtr: 420,
        Line: '5>'
    },
    {
        Node_ID: 420,
        lat: 11.598082,
        lng: 104.883375,
        Path: 'azveA{vc_SSAsPiB{McBsC]yEi@',
        NextPtr: 421,
        Line: '5>'
    },
    {
        Node_ID: 421,
        lat: 11.598075,
        lng: 104.873549,
        Path: 'sfxeAq_d_ScBS?Lr@da@JfHDnCd@jM',
        NextPtr: 422,
        Line: '5>'
    },
    {
        Node_ID: 422,
        lat: 11.599674,
        lng: 104.869636,
        Path: 'mfxeAibb_S`@~M\\`JqKQ',
        NextPtr: 423,
        Line: '5>'
    },
    {
        Node_ID: 423,
        lat: 11.603654,
        lng: 104.869775,
        Path: '}pxeAwha_SgJYsL[',
        NextPtr: 424,
        Line: '5>'
    },
    {
        Node_ID: 424,
        lat: 11.609803,
        lng: 104.870122,
        Path: 'yiyeAmja_SiGMc]_A',
        NextPtr: 425,
        Line: '5>'
    },
    {
        Node_ID: 425,
        lat: 11.615752,
        lng: 104.867355,
        Path: 'gpzeA{la_S}JYe@?cB@i@DwATw@RcA\\i@ZcBbAo@h@y@nA{CtG',
        NextPtr: 426,
        Line: '5>'
    },
    {
        Node_ID: 426,
        lat: 11.618257,
        lng: 104.863141,
        Path: 'au{eAu{`_SmNpY',
        NextPtr: 427,
        Line: '5>'
    },
    {
        Node_ID: 427,
        lat: 11.623102,
        lng: 104.857294,
        Path: 'od|eAca`_SkI~PeC`FwAxBqArAiAt@iAj@u@ZqCp@',
        NextPtr: 428,
        Line: '5>'
    },
    {
        Node_ID: 428,
        lat: 11.628704,
        lng: 104.856304,
        Path: 'gc}eAc|~~RYDuCXuEj@eDX}Ef@wIz@',
        NextPtr: 429,
        Line: '5>'
    },
    {
        Node_ID: 429,
        lat: 11.637638,
        lng: 104.854637,
        Path: 'if~eAyu~~R{KfAsD^QDkJbAmALoOzAyANsBV',
        NextPtr: 430,
        Line: '5>'
    },
    {
        Node_ID: 430,
        lat: 11.645665,
        lng: 104.853198,
        Path: 'c~_fAuk~~Req@|G',
        NextPtr: 431,
        Line: '5>'
    },
    {
        Node_ID: 431,
        lat: 11.649165,
        lng: 104.855663,
        Path: 'kpafAwb~~RcCVe@ByATy@FsEsHcAcBqB{C',
        NextPtr: 432,
        Line: '5>'
    },
    {
        Node_ID: 432,
        lat: 11.65172,
        lng: 104.859375,
        Path: 'sfbfAqr~~RqEiHcCeEiCaEc@s@',
        NextPtr: 433,
        Line: '5>'
    },
    {
        Node_ID: 433,
        lat: 11.669656,
        lng: 104.868755,
        Path: 'wvbfAui__SuDcGo@eASe@@KAOGMKISGQOyAwBsAwBuA{By@oAy@iAIGgAkBy@qAgC}DsAyB}DgGaBeC}CaF}BuDcCyDc@q@uA}BaBmCCWOYs@cAaBuBiBmBo@i@{BkBcCoBq@i@UUMQHY?_@I_@OUWQ]K[?c@HYTQ\\E`@Dd@R\\TR_@bAYn@Yt@W`@GPMl@K\\y@jBU|@]x@]`@INiA|Ci@vAy@zBcCnGiD|Ia@Y',
        NextPtr: null,
        Line: '5>'
    },
    {
        Node_ID: 434,
        lat: 11.65213,
        lng: 104.859364,
        Path: 'geffAyca_SjA}C~BkGv@sBlBaFhA_D^cAJOX_@\\u@V_A~@_CPw@^w@t@gB^_ANDPD\\ATEz@PPHtDpCnAdAnAhA~@`An@p@nAbBr@fAPX\\HrCtEf@z@bDdF`C|DdDjF~AdCtCnElDrFfC~DfAhBBLt@hA|ElIrAxBJVCPDTLHPDTN`@d@f@r@pCpE',
        NextPtr: 435,
        Line: '5<'
    },
    {
        Node_ID: 435,
        lat: 11.649335,
        lng: 104.855457,
        Path: 'axbfAsj__S~AlCl@~@bAxA~ApCNVNNpEpH',
        NextPtr: 436,
        Line: '5<'
    },
    {
        Node_ID: 436,
        lat: 11.645196,
        lng: 104.852877,
        Path: '}fbfA_r~~RxA|BbGtJb@p@JRPEhKeA',
        NextPtr: 437,
        Line: '5<'
    },
    {
        Node_ID: 437,
        lat: 11.636815,
        lng: 104.854439,
        Path: 'smafAqb~~Rls@oH',
        NextPtr: 438,
        Line: '5<'
    },
    {
        Node_ID: 438,
        lat: 11.629203,
        lng: 104.85581,
        Path: 'ey_fAal~~RfGm@dI{@z@E`KkA|De@x@ElFk@',
        NextPtr: 439,
        Line: '5<'
    },
    {
        Node_ID: 439,
        lat: 11.627637,
        lng: 104.856052,
        Path: 'si~eAut~~RzHw@',
        NextPtr: 440,
        Line: '5<'
    },
    {
        Node_ID: 440,
        lat: 11.62351,
        lng: 104.856851,
        Path: 'w_~eAmv~~RXC|BSxDa@lEe@bBOfAOnBS',
        NextPtr: 441,
        Line: '5<'
    },
    {
        Node_ID: 441,
        lat: 11.618196,
        lng: 104.862558,
        Path: '{e}eAa{~~R`@EvDm@n@Uz@a@p@_@pA_AhAkAZa@b@q@Zg@zE}JdEyI',
        NextPtr: 442,
        Line: '5<'
    },
    {
        Node_ID: 442,
        lat: 11.615182,
        lng: 104.867863,
        Path: 'ke|eAo~__SnEaJXg@bFqKlAgCdA}B',
        NextPtr: 443,
        Line: '5<'
    },
    {
        Node_ID: 443,
        lat: 11.609966,
        lng: 104.869913,
        Path: 'ir{eAo_a_SN[Vk@x@uAhA{ALOfBcAd@YfA_@j@OvAUd@CbBC`KV',
        NextPtr: 444,
        Line: '5<'
    },
    {
        Node_ID: 444,
        lat: 11.604033,
        lng: 104.869497,
        Path: 'iqzeAmla_S|Vr@hFNJ?nDH',
        NextPtr: 445,
        Line: '5<'
    },
    {
        Node_ID: 445,
        lat: 11.598259,
        lng: 104.869089,
        Path: 'elyeA_ja_SpMZrHVpADhHL',
        NextPtr: 446,
        Line: '5<'
    },
    {
        Node_ID: 446,
        lat: 11.597895,
        lng: 104.883205,
        Path: 'chxeAwga_SvB@?M]aJ[{KW_HCYMaEc@{XMmGS{L|BV',
        NextPtr: 447,
        Line: '5<'
    },
    {
        Node_ID: 447,
        lat: 11.591287,
        lng: 104.881822,
        Path: 'wexeAk_d_SzD`@zMbBvC^fD^dBPfFj@',
        NextPtr: 448,
        Line: '5<'
    },
    {
        Node_ID: 448,
        lat: 11.584976,
        lng: 104.882054,
        Path: 'q|veAgwc_S~@JR@T?j@A~@G@NtBEt@?lEIf@AlDInK[t@A',
        NextPtr: 449,
        Line: '5<'
    },
    {
        Node_ID: 449,
        lat: 11.580906,
        lng: 104.882628,
        Path: 'cuueAmxc_Sv@AbCGjCGdCGtACTA`@E|@SpCu@',
        NextPtr: 450,
        Line: '5<'
    },
    {
        Node_ID: 450,
        lat: 11.5809,
        lng: 104.886266,
        Path: 'w{teA}{c_SVIGu@GkDMeF?yD?{@',
        NextPtr: 451,
        Line: '5<'
    },
    {
        Node_ID: 451,
        lat: 11.582447,
        lng: 104.888252,
        Path: '}{teAerd_SEqFmDL_B?Ba@c@cC',
        NextPtr: 452,
        Line: '5<'
    },
    {
        Node_ID: 452,
        lat: 11.582975,
        lng: 104.890852,
        Path: 'qeueAo~d_SeC{N',
        NextPtr: 453,
        Line: '5<'
    },
    {
        Node_ID: 453,
        lat: 11.579254,
        lng: 104.894032,
        Path: 'uiueAkne_Sc@gC~CcBxG_EtHeEx@a@',
        NextPtr: 454,
        Line: '5<'
    },
    {
        Node_ID: 454,
        lat: 11.576717,
        lng: 104.899071,
        Path: 'oqteA_cf_Sx@_@d@WNMHUdA{CdBqFjDyKf@eB',
        NextPtr: 455,
        Line: '5<'
    },
    {
        Node_ID: 455,
        lat: 11.571118169241425,
        lng: 104.8995984759132,
        Path: 'uateAibg_S^mAvBJpGZbDR|DNd@@tD_A~@U|@Q',
        NextPtr: 456,
        Line: '5<'
    },
    {
        Node_ID: 456,
        lat: 11.565942,
        lng: 104.900464,
        Path: 'o~reAqeg_SfDw@nA_@h@IPENCf@?x@CvBK~AMzCSNA`FS',
        NextPtr: 457,
        Line: '5<'
    },
    {
        Node_ID: 457,
        lat: 11.560545,
        lng: 104.901084,
        Path: 'c~qeAekg_SXA|@G`AG~@G`AC`EW~@I`CInBMxF]n@E',
        NextPtr: 458,
        Line: '5<'
    },
    {
        Node_ID: 458,
        lat: 11.553421,
        lng: 104.901936,
        Path: 'g|peAaog_SvCSdKg@~Ku@hCQfAINChCObBI',
        NextPtr: 459,
        Line: '5<'
    },
    {
        Node_ID: 459,
        lat: 11.549554,
        lng: 104.904954,
        Path: 'yooeAmtg_S`EUxAM\\ELMnBmCt@cAtAmB\\]vC_E',
        NextPtr: 460,
        Line: '5<'
    },
    {
        Node_ID: 460,
        lat: 11.546964,
        lng: 104.908179,
        Path: '_xneAcgh_SfLqO`BuB',
        NextPtr: 461,
        Line: '5<'
    },
    {
        Node_ID: 461,
        lat: 11.544217,
        lng: 104.91195,
        Path: 'ugneAk{h_SvByCb@k@zB{CxAgB~@uAhAuBRg@Ro@',
        NextPtr: 462,
        Line: '5<'
    },
    {
        Node_ID: 462,
        lat: 11.543876,
        lng: 104.916072,
        Path: 'qvmeA{ri_SNg@Pu@VaBBW@KHiDIsHAoB',
        NextPtr: 463,
        Line: '5<'
    },
    {
        Node_ID: 463,
        lat: 11.543942,
        lng: 104.921902,
        Path: 'otmeAklj_S_@wc@',
        NextPtr: 464,
        Line: '5<'
    },
    {
        Node_ID: 464,
        lat: 11.544071,
        lng: 104.923831,
        Path: 'qtmeAmlj_SC{@AaAIeMMiOEyCASEsG',
        NextPtr: 465,
        Line: '5<'
    },
    {
        Node_ID: 465,
        lat: 11.544092,
        lng: 104.927104,
        Path: '}umeA}|k_SCcE?g@?cAI}I',
        NextPtr: 466,
        Line: '5<'
    },
    {
        Node_ID: 466,
        lat: 11.544675,
        lng: 104.933011,
        Path: 'kvmeAkql_SAiDBq@BYF{AEw@Sq@kBkCUSSMs@WsBo@`BoGHIXSVYXk@h@}BLD',
        NextPtr: 467,
        Line: '5<'
    },
    {
        Node_ID: 467,
        lat: 11.547112,
        lng: 104.934816,
        Path: 'symeAovm_S`@}AN_@q@SgEcAoAY}@WaAY]Ok@Y',
        NextPtr: null,
        Line: '5<'
    },
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    {
        Node_ID: 500,
        lat: 11.59075,
        lng: 104.928604,
        Path: '',
        NextPtr: 501,
        Line: '6>'
    },
    {
        Node_ID: 501,
        lat: 11.601638,
        lng: 104.930717,
        Path: 'eyveAwzl_SwU{KaGc@gd@V',
        NextPtr: 502,
        Line: '6>'
    },
    {
        Node_ID: 502,
        lat: 11.605174,
        lng: 104.929837,
        Path: 'g}xeA_hm_SaUnD',
        NextPtr: 503,
        Line: '6>'
    },
    {
        Node_ID: 503,
        lat: 11.608358,
        lng: 104.929069,
        Path: 'isyeAobm_S}RxC',
        NextPtr: 504,
        Line: '6>'
    },
    {
        Node_ID: 504,
        lat: 11.612381,
        lng: 104.928076,
        Path: 'ggzeAu}l_ScXdE',
        NextPtr: 505,
        Line: '6>'
    },
    {
        Node_ID: 505,
        lat: 11.615405,
        lng: 104.927367,
        Path: 'k`{eAowl_S}QlC',
        NextPtr: 506,
        Line: '6>'
    },
    {
        Node_ID: 506,
        lat: 11.620346,
        lng: 104.926106,
        Path: 'is{eAasl_S{]zF',
        NextPtr: 507,
        Line: '6>'
    },
    {
        Node_ID: 507,
        lat: 11.630308,
        lng: 104.923025,
        Path: 'er|eAekl_Swo@nKoLxE',
        NextPtr: 508,
        Line: '6>'
    },
    {
        Node_ID: 508,
        lat: 11.636107,
        lng: 104.920407,
        Path: 'mp~eA{wk_SuU~JqLhC',
        NextPtr: 509,
        Line: '6>'
    },
    {
        Node_ID: 509,
        lat: 11.641151,
        lng: 104.91844,
        Path: 'ut_fAqgk_So^hK',
        NextPtr: 510,
        Line: '6>'
    },
    {
        Node_ID: 510,
        lat: 11.653276,
        lng: 104.913997,
        Path: 'et`fAg{j_Sor@pViWdC',
        NextPtr: 511,
        Line: '6>'
    },
    {
        Node_ID: 511,
        lat: 11.658965,
        lng: 104.913243,
        Path: '_`cfAo_j_S}X~CsHG',
        NextPtr: 512,
        Line: '6>'
    },
    {
        Node_ID: 512,
        lat: 11.670522,
        lng: 104.915097,
        Path: 'qcdfAwzi_Suj@FwGmAwRmH',
        NextPtr: 513,
        Line: '6>'
    },
    {
        Node_ID: 513,
        lat: 11.675465,
        lng: 104.91718,
        Path: 'wkffAkfj_S}]_L',
        NextPtr: 514,
        Line: '6>'
    },
    {
        Node_ID: 514,
        lat: 11.680792,
        lng: 104.920293,
        Path: 'ujgfAksj_SyOwEqEmC{HgG',
        NextPtr: 515,
        Line: '6>'
    },
    {
        Node_ID: 515,
        lat: 11.682502,
        lng: 104.919111,
        Path: '}khfAyfk_SwCcBgDvI',
        NextPtr: 516,
        Line: '6>'
    },
    {
        Node_ID: 516,
        lat: 11.688096,
        lng: 104.890484,
        Path: 'avhfAa_k_S{m@v_BcBxN]rMVnL|AjKbD`LxEfK',
        NextPtr: 517,
        Line: '6>'
    },
    {
        Node_ID: 517,
        lat: 11.684785,
        lng: 104.886622,
        Path: 'gyifAsle_SzSpV',
        NextPtr: 518,
        Line: '6>'
    },
    {
        Node_ID: 518,
        lat: 11.680791,
        lng: 104.883623,
        Path: 'odifAaud_ShOfLrGnD',
        NextPtr: null,
        Line: '6>'
    },
    {
        Node_ID: 519,
        lat: 11.684736,
        lng: 104.887235,
        Path: '',
        NextPtr: 520,
        Line: '6<'
    },
    {
        Node_ID: 520,
        lat: 11.680656,
        lng: 104.919815,
        Path: '}difAwwd_SiP_RgGiLsBiGqB{IqA_P\\qO|A}LbEgN~l@o}AnDdC',
        NextPtr: 521,
        Line: '6<'
    },
    {
        Node_ID: 521,
        lat: 11.675136,
        lng: 104.916739,
        Path: '}jhfAcdk_SlMlIzR`H',
        NextPtr: 522,
        Line: '6<'
    },
    {
        Node_ID: 522,
        lat: 11.670484,
        lng: 104.914691,
        Path: 'shgfAspj_Sd\\hK',
        NextPtr: 523,
        Line: '6<'
    },
    {
        Node_ID: 523,
        lat: 11.657688,
        lng: 104.912959,
        Path: 'kkffAidj_SnY`Jdt@I',
        NextPtr: 524,
        Line: '6<'
    },
    {
        Node_ID: 524,
        lat: 11.653102,
        lng: 104.913766,
        Path: 'q{cfAuyi_St[sC',
        NextPtr: 525,
        Line: '6<'
    },
    {
        Node_ID: 525,
        lat: 11.648231,
        lng: 104.914951,
        Path: '}~bfAg~i_SnWaD~DcA',
        NextPtr: 526,
        Line: '6<'
    },
    {
        Node_ID: 526,
        lat: 11.640115,
        lng: 104.918623,
        Path: 'm`bfAmej_Srq@iV',
        NextPtr: 527,
        Line: '6<'
    },
    {
        Node_ID: 527,
        lat: 11.630094,
        lng: 104.922868,
        Path: 'um`fAk|j_S~e@{MnVyJ',
        NextPtr: 528,
        Line: '6<'
    },
    {
        Node_ID: 528,
        lat: 11.624827,
        lng: 104.92465,
        Path: 'ao~eAqwk_S~HcDxUaE',
        NextPtr: 529,
        Line: '6<'
    },
    {
        Node_ID: 529,
        lat: 11.62401,
        lng: 104.924928,
        Path: 'gn}eAsbl_SdDe@',
        NextPtr: 530,
        Line: '6<'
    },
    {
        Node_ID: 530,
        lat: 11.61974,
        lng: 104.92598,
        Path: 'ai}eAycl_SrYyE',
        NextPtr: 531,
        Line: '6<'
    },
    {
        Node_ID: 531,
        lat: 11.616103,
        lng: 104.926858,
        Path: 'kn|eAkjl_SrU_E',
        NextPtr: 532,
        Line: '6<'
    },
    {
        Node_ID: 532,
        lat: 11.612355,
        lng: 104.927771,
        Path: 'uw{eAepl_SjV_E',
        NextPtr: 533,
        Line: '6<'
    },
    {
        Node_ID: 533,
        lat: 11.608212,
        lng: 104.928764,
        Path: 'k`{eAcvl_S|XiE',
        NextPtr: 534,
        Line: '6<'
    },
    {
        Node_ID: 534,
        lat: 11.604738,
        lng: 104.929672,
        Path: 'mfzeAk|l_SvTmD',
        NextPtr: 536,
        Line: '6<'
    },
    null,
    {
        Node_ID: 536,
        lat: 11.601319,
        lng: 104.930484,
        Path: 'upyeA{am_ShTcD',
        NextPtr: 537,
        Line: '6<'
    },
    {
        Node_ID: 537,
        lat: 11.597086,
        lng: 104.930584,
        Path: '_~xeAmfm_S~Fe@dT?',
        NextPtr: 538,
        Line: '6<'
    },
    {
        Node_ID: 538,
        lat: 11.591164,
        lng: 104.9285,
        Path: 'y`xeAsgm_S|LFnFzAxNzH',
        NextPtr: null,
        Line: '6<'
    },
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    {
        Node_ID: 600,
        lat: 11.637333,
        lng: 104.883844,
        Path: '',
        NextPtr: 601,
        Line: '7>'
    },
    {
        Node_ID: 601,
        lat: 11.630283,
        lng: 104.884529,
        Path: '{{_fAmcd_S~C|Dn@z@vAtBl@z@b@j@^`@VRLFNDJ?HAJC^WbBoAp@e@jDiC\\UhBsA`As@NKpBuAlCgBv@g@',
        NextPtr: 602,
        Line: '7>'
    },
    {
        Node_ID: 602,
        lat: 11.627689,
        lng: 104.886387,
        Path: 'up~eA_hd_StD_CpAw@lAu@`@UtCgB',
        NextPtr: 603,
        Line: '7>'
    },
    {
        Node_ID: 603,
        lat: 11.622583,
        lng: 104.890017,
        Path: 'g`~eAksd_SpBoAdBgA`@SjHsEpCmB`C}A~DmC',
        NextPtr: 604,
        Line: '7>'
    },
    {
        Node_ID: 604,
        lat: 11.618634,
        lng: 104.892441,
        Path: 'm`}eAeje_SdAu@hBiA|@i@ZWz@i@t@a@vAk@rBy@`Bq@nBy@',
        NextPtr: 605,
        Line: '7>'
    },
    {
        Node_ID: 605,
        lat: 11.614039,
        lng: 104.894713,
        Path: 'sg|eAeye_SrMiFtB{@bDqAfDsA',
        NextPtr: 606,
        Line: '7>'
    },
    {
        Node_ID: 606,
        lat: 11.610544,
        lng: 104.896443,
        Path: '}j{eAqgf_SnBy@fCcAlEeBhBs@hCcA',
        NextPtr: 607,
        Line: '7>'
    },
    {
        Node_ID: 607,
        lat: 11.606334,
        lng: 104.898451,
        Path: 'euzeAmrf_S`CaArJwDnFwBf@Q|@U',
        NextPtr: 608,
        Line: '7>'
    },
    {
        Node_ID: 608,
        lat: 11.600104,
        lng: 104.899907,
        Path: 'yzyeAg_g_SVG|Dk@lImAhK}ArDi@~BY',
        NextPtr: 609,
        Line: '7>'
    },
    {
        Node_ID: 609,
        lat: 11.593153,
        lng: 104.899691,
        Path: 'wsxeAkhg_SpC[^GbBMjEMh@GZAjBYt@Qz@IV?|CN|APdANdAPdAZz@`@|@b@TN',
        NextPtr: 610,
        Line: '7>'
    },
    {
        Node_ID: 610,
        lat: 11.583876,
        lng: 104.887608,
        Path: '{gweAufg_S|@l@f@d@Z^r@bA|EjI~@`ApBhDfF|I`AlBrCvEtApBlC`E`@j@lC`En@bAjAfBLPl@r@d@d@x@p@d@\\lBjA',
        NextPtr: 611,
        Line: '7>'
    },
    {
        Node_ID: 611,
        lat: 11.578223,
        lng: 104.887515,
        Path: 'wmueAm{d_Sd@TZNd@NdAHrBD|A?pDM|HOjFI',
        NextPtr: 612,
        Line: '7>'
    },
    {
        Node_ID: 612,
        lat: 11.573476,
        lng: 104.887758,
        Path: '{jteAozd_ShDIfCGpISnHK',
        NextPtr: 613,
        Line: '7>'
    },
    {
        Node_ID: 613,
        lat: 11.567767,
        lng: 104.888105,
        Path: 'gmseAa|d_SbDGlGM~DKBBF@tIa@p@CnBG',
        NextPtr: 614,
        Line: '7>'
    },
    {
        Node_ID: 614,
        lat: 11.564852,
        lng: 104.888619,
        Path: 'sireAk~d_S`EOrEYr@G^GxAi@',
        NextPtr: 615,
        Line: '7>'
    },
    {
        Node_ID: 615,
        lat: 11.560568,
        lng: 104.889017,
        Path: 'owqeAoae_Sr@]nEiBpDwAhA_@pAk@JEJCVGFAH?L@b@Xt@l@fClCg@j@',
        NextPtr: 616,
        Line: '7>'
    },
    {
        Node_ID: 616,
        lat: 11.557396,
        lng: 104.887046,
        Path: 'm{peAyde_Sb@d@PNLN|@x@VVVRpA|@bAj@rAx@lAd@l@Tv@RJB',
        NextPtr: 617,
        Line: '7>'
    },
    {
        Node_ID: 617,
        lat: 11.552158,
        lng: 104.887685,
        Path: 'qhpeAuwd_SlDt@TDRDl@FR@v@CdDWnE[|@KVEnFyB',
        NextPtr: 618,
        Line: '7>'
    },
    {
        Node_ID: 618,
        lat: 11.549552,
        lng: 104.89067,
        Path: 'choeAm{d_St@[z@k@|@q@`A}@rAaBfAqBv@_BXm@Xw@',
        NextPtr: 619,
        Line: '7>'
    },
    {
        Node_ID: 619,
        lat: 11.549435,
        lng: 104.895639,
        Path: 'ixneAane_SJWHm@@W?i@C_FMeNCk@Ai@@SDSP[',
        NextPtr: 620,
        Line: '7>'
    },
    {
        Node_ID: 620,
        lat: 11.54538,
        lng: 104.899693,
        Path: 'owneAimf_S|EsDT[F_@@YSwAEs@Be@Rk@d@k@t@YpBUbBc@|@[hAk@r@e@jAiA',
        NextPtr: 621,
        Line: '7>'
    },
    {
        Node_ID: 621,
        lat: 11.543505,
        lng: 104.904039,
        Path: 'y}meAifg_S`AcA|AuB~@kBx@{Bb@uB\\kDN}C',
        NextPtr: 622,
        Line: '7>'
    },
    {
        Node_ID: 622,
        lat: 11.541769,
        lng: 104.908046,
        Path: 'ormeAkah_SPmDDq@b@_C^yAZ{@j@mAt@oAdAqAfAeA',
        NextPtr: 623,
        Line: '7>'
    },
    {
        Node_ID: 623,
        lat: 11.532659,
        lng: 104.913022,
        Path: 'ggmeAwzh_Sz@s@|@m@h@UnVqKd[_N',
        NextPtr: 624,
        Line: '7>'
    },
    {
        Node_ID: 624,
        lat: 11.52831,
        lng: 104.915545,
        Path: 'mnkeAazi_SjDyA`Bq@rJeExAs@ZOtAaAXY',
        NextPtr: 625,
        Line: '7>'
    },
    {
        Node_ID: 625,
        lat: 11.526505,
        lng: 104.919889,
        Path: 'esjeAsij_SPQv@y@d@o@~AeCf@mAL]f@qBVmAR_BBSByAAyA',
        NextPtr: null,
        Line: '7>'
    },
    {
        Node_ID: 626,
        lat: 11.528065,
        lng: 104.923986,
        Path: '',
        NextPtr: 627,
        Line: '7<'
    },
    {
        Node_ID: 627,
        lat: 11.526797,
        lng: 104.918871,
        Path: '{pjeAg~k_SvClJ`@jAj@zBZlDD~CEvBAP',
        NextPtr: 628,
        Line: '7<'
    },
    {
        Node_ID: 628,
        lat: 11.529257,
        lng: 104.915138,
        Path: '{hjeAy}j_SGl@Kl@]dBc@zAc@dAUb@y@rAcArAa@f@c@b@eAx@i@\\u@`@',
        NextPtr: 629,
        Line: '7<'
    },
    {
        Node_ID: 629,
        lat: 11.533053,
        lng: 104.913196,
        Path: 'sxjeAcfj_S}At@uSvI',
        NextPtr: 630,
        Line: '7<'
    },
    {
        Node_ID: 630,
        lat: 11.537468,
        lng: 104.910842,
        Path: 'gpkeAwyi_SyBbAcBr@gGjCcEfB{An@mBz@',
        NextPtr: 631,
        Line: '7<'
    },
    {
        Node_ID: 631,
        lat: 11.541892,
        lng: 104.908283,
        Path: '{kleAaki_S}Ar@cKnE_FvBa@Pa@Rc@ViA~@',
        NextPtr: 632,
        Line: '7<'
    },
    {
        Node_ID: 632,
        lat: 11.543682,
        lng: 104.9048,
        Path: 'ogmeAe{h_S]X{@|@}@jAi@`A[j@Uf@a@hAm@hCU|ACz@',
        NextPtr: 633,
        Line: '7<'
    },
    {
        Node_ID: 633,
        lat: 11.544188,
        lng: 104.902015,
        Path: 'mrmeA}eh_SO`DMpCQlB_@~BIZ',
        NextPtr: 634,
        Line: '7<'
    },
    {
        Node_ID: 634,
        lat: 11.54538,
        lng: 104.899693,
        Path: 'gumeA_tg_SQn@e@pAcAlB_AtAo@p@k@j@',
        NextPtr: 635,
        Line: '7<'
    },
    {
        Node_ID: 635,
        lat: 11.54955,
        lng: 104.895896,
        Path: '_~meA{fg_Sq@n@{@p@{Az@sAf@uA\\uANm@Jc@R[VWb@KZCHG`@Bd@XbB?VI\\U^SRKFkCrBWR',
        NextPtr: 636,
        Line: '7<'
    },
    {
        Node_ID: 636,
        lat: 11.550089,
        lng: 104.890292,
        Path: 'mwneA_nf_SSRKPGTAPAPBd@TnUAzAKl@Sb@m@|A',
        NextPtr: 637,
        Line: '7<'
    },
    {
        Node_ID: 637,
        lat: 11.552306,
        lng: 104.887883,
        Path: 'ozneA}je_Sy@dBgAjBaAnAuAnAsBpA{@^',
        NextPtr: 638,
        Line: '7<'
    },
    {
        Node_ID: 638,
        lat: 11.556443,
        lng: 104.887129,
        Path: 'yhoeAy{d_S{CpAcA`@SDyAN{E\\{BP_A@o@Go@M',
        NextPtr: 639,
        Line: '7<'
    },
    {
        Node_ID: 639,
        lat: 11.564381,
        lng: 104.889244,
        Path: '_cpeAsvd_Sa@Mk@ImDw@k@Ss@[a@SeEgCi@a@uCsCuAyAuBqBGGMG]AQDiA^mC`AiBt@uB|@qAh@',
        NextPtr: 640,
        Line: '7<'
    },
    {
        Node_ID: 640,
        lat: 11.568791,
        lng: 104.888342,
        Path: 'atqeA}ce_SgDrA_@Ni@Jq@FgCNgCLuCJoDL',
        NextPtr: 641,
        Line: '7<'
    },
    {
        Node_ID: 641,
        lat: 11.574095,
        lng: 104.887886,
        Path: '}oreAm~d_SgCL_DRI?EF_EHoGN{GJ',
        NextPtr: 642,
        Line: '7<'
    },
    {
        Node_ID: 642,
        lat: 11.578122,
        lng: 104.887698,
        Path: 'aqseA}{d_SiDFaNXyCH',
        NextPtr: 643,
        Line: '7<'
    },
    {
        Node_ID: 643,
        lat: 11.579727,
        lng: 104.887666,
        Path: 'gjteAqzd_S_IN',
        NextPtr: 644,
        Line: '7<'
    },
    {
        Node_ID: 644,
        lat: 11.586675,
        lng: 104.891106,
        Path: 'itteAazd_SuFJyDLsA?sBEgAGGAe@QYOc@Q}BwAmA_Ak@k@o@w@mBwCaD}EW]',
        NextPtr: 645,
        Line: '7<'
    },
    {
        Node_ID: 645,
        lat: 11.587528,
        lng: 104.892422,
        Path: 'e`veA{oe_SwD{F',
        NextPtr: 646,
        Line: '7<'
    },
    {
        Node_ID: 646,
        lat: 11.591894,
        lng: 104.898708,
        Path: '}eveAuwe_Su@eAwAcCeAeBy@}AuDuGeDwFu@eAs@wAcCaE',
        NextPtr: 647,
        Line: '7<'
    },
    {
        Node_ID: 647,
        lat: 11.600768,
        lng: 104.900106,
        Path: 'u`weAq_g_SOUS_@s@eAa@a@c@_@a@_@o@_@cB_A}Ac@wCi@wAMiCOiABkANwBb@cBJ{BDw@D}BRkDb@cBR',
        NextPtr: 648,
        Line: '7<'
    },
    {
        Node_ID: 648,
        lat: 11.604738,
        lng: 104.899221,
        Path: 'uwxeA_hg_SuDf@kEn@aEj@uEr@C[',
        NextPtr: 649,
        Line: '7<'
    },
    {
        Node_ID: 649,
        lat: 11.610457,
        lng: 104.896851,
        Path: 'opyeAgbg_SwHjAyA\\mFpBoNzF',
        NextPtr: 650,
        Line: '7<'
    },
    {
        Node_ID: 650,
        lat: 11.614091,
        lng: 104.894986,
        Path: '_tzeAosf_ScBp@kAb@aA^kDnAqEnBoBr@',
        NextPtr: 651,
        Line: '7<'
    },
    {
        Node_ID: 651,
        lat: 11.618689,
        lng: 104.892612,
        Path: 'ak{eAchf_ScDtAeDrAcCbAkJtD{Al@',
        NextPtr: 652,
        Line: '7<'
    },
    {
        Node_ID: 652,
        lat: 11.622293,
        lng: 104.890642,
        Path: 'wg|eAqye_SyElBkBt@{Ah@w@`@aElCe@\\',
        NextPtr: 653,
        Line: '7<'
    },
    {
        Node_ID: 653,
        lat: 11.630917,
        lng: 104.884447,
        Path: 'y}|eAule_SkPxKsGbEeAl@aBdAaHdEcCzAuD|B_CzA',
        NextPtr: 654,
        Line: '7<'
    },
    {
        Node_ID: 654,
        lat: 11.633997,
        lng: 104.882147,
        Path: '}s~eAgfd_SWPyJ|G_@VaD`CMF',
        NextPtr: 655,
        Line: '7<'
    },
    {
        Node_ID: 655,
        lat: 11.637078,
        lng: 104.883936,
        Path: '_g_fAswc_SoCtBSLQHQ@OCSMi@m@}DoFoDqE',
        NextPtr: null,
        Line: '7<'
    },
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    {
        Node_ID: 700,
        lat: 11.567891,
        lng: 104.853707,
        Path: '',
        NextPtr: 701,
        Line: '8>'
    },
    {
        Node_ID: 701,
        lat: 11.588644,
        lng: 104.854065,
        Path: 'ijreAmf~~RyLA_OgAoRbAaRl@aPDsk@HCiC',
        NextPtr: 702,
        Line: '8>'
    },
    {
        Node_ID: 702,
        lat: 11.589034,
        lng: 104.860746,
        Path: 'klveA}h~~RaAwh@',
        NextPtr: 703,
        Line: '8>'
    },
    {
        Node_ID: 703,
        lat: 11.589145,
        lng: 104.863768,
        Path: 'coveAur__SQ{Q',
        NextPtr: 704,
        Line: '8>'
    },
    {
        Node_ID: 704,
        lat: 11.589471,
        lng: 104.8691,
        Path: '{oveAye`_So@a`@',
        NextPtr: 705,
        Line: '8>'
    },
    {
        Node_ID: 705,
        lat: 11.589648,
        lng: 104.873052,
        Path: 'sqveAoga_S_@uW',
        NextPtr: 706,
        Line: '8>'
    },
    {
        Node_ID: 706,
        lat: 11.589828,
        lng: 104.878016,
        Path: 'srveAw_b_Si@a^',
        NextPtr: 707,
        Line: '8>'
    },
    {
        Node_ID: 707,
        lat: 11.590099,
        lng: 104.883595,
        Path: '}sveAu~b_S_Aya@',
        NextPtr: 708,
        Line: '8>'
    },
    {
        Node_ID: 708,
        lat: 11.590418,
        lng: 104.888812,
        Path: '}uveAqad_S}@q_@',
        NextPtr: 709,
        Line: '8>'
    },
    {
        Node_ID: 709,
        lat: 11.590635,
        lng: 104.894489,
        Path: 'ywveAabe_Su@ob@',
        NextPtr: 710,
        Line: '8>'
    },
    {
        Node_ID: 710,
        lat: 11.589374,
        lng: 104.897992,
        Path: 'kyveAqef_SIwMX[Am@`GoD',
        NextPtr: 711,
        Line: '8>'
    },
    {
        Node_ID: 711,
        lat: 11.586138,
        lng: 104.900158,
        Path: 'upveA}{f_SbSsL',
        NextPtr: 712,
        Line: '8>'
    },
    {
        Node_ID: 712,
        lat: 11.584063,
        lng: 104.905124,
        Path: 'm|ueAiig_S`Cq@OmAnHuY',
        NextPtr: 713,
        Line: '8>'
    },
    {
        Node_ID: 713,
        lat: 11.583975,
        lng: 104.910138,
        Path: 'koueA_hh_SlCiMiCeP',
        NextPtr: 714,
        Line: '8>'
    },
    {
        Node_ID: 714,
        lat: 11.584843,
        lng: 104.914017,
        Path: 'goueAigi_SiDoQWkC',
        NextPtr: null,
        Line: '8>'
    },
    {
        Node_ID: 715,
        lat: 11.58524,
        lng: 104.91434,
        Path: '',
        NextPtr: 716,
        Line: '8<'
    },
    {
        Node_ID: 716,
        lat: 11.584211,
        lng: 104.905292,
        Path: 'wvueAsaj_S~@fI`Ht_@sCrL',
        NextPtr: 717,
        Line: '8<'
    },
    {
        Node_ID: 717,
        lat: 11.585543,
        lng: 104.901496,
        Path: 'ipueAaih_SiGtV',
        NextPtr: 718,
        Line: '8<'
    },
    {
        Node_ID: 718,
        lat: 11.589996,
        lng: 104.89791,
        Path: 'sxueAkqg_S[pCV|@{BL{VnN',
        NextPtr: 719,
        Line: '8<'
    },
    {
        Node_ID: 719,
        lat: 11.590981,
        lng: 104.894108,
        Path: 'otveA}zf_ScF|Ch@bABrO',
        NextPtr: 720,
        Line: '8<'
    },
    {
        Node_ID: 720,
        lat: 11.590724,
        lng: 104.889342,
        Path: 'ezveAgcf_Sh@z\\',
        NextPtr: 721,
        Line: '8<'
    },
    {
        Node_ID: 721,
        lat: 11.590393,
        lng: 104.88274,
        Path: '{xveAkee_SnAfh@',
        NextPtr: 722,
        Line: '8<'
    },
    {
        Node_ID: 722,
        lat: 11.590199,
        lng: 104.87877,
        Path: 'kvveAc|c_Sl@vW',
        NextPtr: 723,
        Line: '8<'
    },
    {
        Node_ID: 723,
        lat: 11.589963,
        lng: 104.87428,
        Path: '}tveAkcc_Sb@xZ',
        NextPtr: 724,
        Line: '8<'
    },
    {
        Node_ID: 724,
        lat: 11.589758,
        lng: 104.870256,
        Path: 'ysveAqgb_Sb@jX',
        NextPtr: 725,
        Line: '8<'
    },
    {
        Node_ID: 725,
        lat: 11.589455,
        lng: 104.864668,
        Path: 'urveAena_SbA|a@',
        NextPtr: 726,
        Line: '8<'
    },
    {
        Node_ID: 726,
        lat: 11.589223,
        lng: 104.861328,
        Path: 'qpveAgk`_S\\|S',
        NextPtr: 727,
        Line: '8<'
    },
    {
        Node_ID: 727,
        lat: 11.588828,
        lng: 104.854422,
        Path: 'soveAiv__SlAdj@',
        NextPtr: 728,
        Line: '8<'
    },
    {
        Node_ID: 728,
        lat: 11.587389,
        lng: 104.853219,
        Path: 'emveAck~~RLvEnG@',
        NextPtr: 729,
        Line: '8<'
    },
    {
        Node_ID: 729,
        lat: 11.57937,
        lng: 104.853333,
        Path: 'gdveAid~~Rvp@G',
        NextPtr: 730,
        Line: '8<'
    },
    {
        Node_ID: 730,
        lat: 11.574291,
        lng: 104.853734,
        Path: 'orteAqd~~RzSk@|Im@',
        NextPtr: 731,
        Line: '8<'
    },
    {
        Node_ID: 731,
        lat: 11.568723,
        lng: 104.853526,
        Path: 'krseAig~~R`I_@bDTbNx@nC@',
        NextPtr: 732,
        Line: '8<'
    },
    {
        Node_ID: 732,
        lat: 11.562385,
        lng: 104.852982,
        Path: 'coreA}e~~RxB?~CR`I`AxG@pJT',
        NextPtr: null,
        Line: '8<'
    },
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    {
        Node_ID: 800,
        lat: 11.519293,
        lng: 104.77881,
        Path: '',
        NextPtr: 801,
        Line: '9>'
    },
    {
        Node_ID: 801,
        lat: 11.514149,
        lng: 104.77933,
        Path: 'ozheAgso~RvEaApEs@dPT',
        NextPtr: 802,
        Line: '9>'
    },
    {
        Node_ID: 802,
        lat: 11.504327,
        lng: 104.77945,
        Path: '}{geAgvo~RzWb@tED~@Ph@h@j@h@zA?lAaA`BWnO}@',
        NextPtr: 803,
        Line: '9>'
    },
    {
        Node_ID: 803,
        lat: 11.499191,
        lng: 104.780014,
        Path: '}~eeAovo~Rp`@mC',
        NextPtr: 804,
        Line: '9>'
    },
    {
        Node_ID: 804,
        lat: 11.493669,
        lng: 104.780931,
        Path: 'i}deA{zo~Rja@iD',
        NextPtr: 805,
        Line: '9>'
    },
    {
        Node_ID: 805,
        lat: 11.48939,
        lng: 104.781707,
        Path: 'a{ceAe`p~RtY_D',
        NextPtr: 806,
        Line: '9>'
    },
    {
        Node_ID: 806,
        lat: 11.484679,
        lng: 104.782681,
        Path: 'i`ceAgep~R|KmApOeB',
        NextPtr: 807,
        Line: '9>'
    },
    {
        Node_ID: 807,
        lat: 11.479639,
        lng: 104.783367,
        Path: 'wbbeA{jp~RvScCfEQdDC',
        NextPtr: 808,
        Line: '9>'
    },
    {
        Node_ID: 808,
        lat: 11.472502,
        lng: 104.783377,
        Path: 'sbaeAqop~R|ZCxO?',
        NextPtr: 809,
        Line: '9>'
    },
    {
        Node_ID: 809,
        lat: 11.468337,
        lng: 104.783369,
        Path: 'wu_eAuop~RlXH',
        NextPtr: 810,
        Line: '9>'
    },
    {
        Node_ID: 810,
        lat: 11.467919,
        lng: 104.784644,
        Path: 'g|~dAgop~RzBFYeDOkA',
        NextPtr: 811,
        Line: '9>'
    },
    {
        Node_ID: 811,
        lat: 11.464756,
        lng: 104.793529,
        Path: 'oy~dAsvp~RgAcFc@uCCkCRaCL{HHiDJwANqAd@oAzA{AnE_F`I{B',
        NextPtr: 812,
        Line: '9>'
    },
    {
        Node_ID: 812,
        lat: 11.461833,
        lng: 104.796426,
        Path: 'ef~dAqor~RhHeCjAg@v@s@xC_I',
        NextPtr: 813,
        Line: '9>'
    },
    {
        Node_ID: 813,
        lat: 11.459951,
        lng: 104.801957,
        Path: '_t}dAu`s~RjAsDtBqFRk@~@wDtAyIXcB',
        NextPtr: 814,
        Line: '9>'
    },
    {
        Node_ID: 814,
        lat: 111.460185,
        lng: 104.807829,
        Path: 'yg}dA{bt~RXoCLeCQkI}@mHQ}B?oAHiA',
        NextPtr: 815,
        Line: '9>'
    },
    {
        Node_ID: 815,
        lat: 11.459821,
        lng: 104.811831,
        Path: 'ii}dA_hu~R`AkM?cG@aB',
        NextPtr: 816,
        Line: '9>'
    },
    {
        Node_ID: 816,
        lat: 11.459729,
        lng: 104.812354,
        Path: 'eg}dAoav~RDkBHELJ',
        NextPtr: 817,
        Line: '9>'
    },
    {
        Node_ID: 817,
        lat: 11.449137,
        lng: 104.806287,
        Path: 'ef}dAudv~Rd\\`PdNjGhTfK',
        NextPtr: 818,
        Line: '9>'
    },
    {
        Node_ID: 818,
        lat: 11.449014,
        lng: 104.806469,
        Path: '',
        NextPtr: 819,
        Line: '9>'
    },
    {
        Node_ID: 819,
        lat: 11.453078,
        lng: 104.808808,
        Path: 'kd{dAm_u~RwVuL',
        NextPtr: 820,
        Line: '9>'
    },
    {
        Node_ID: 820,
        lat: 11.459304,
        lng: 104.812429,
        Path: 'u|{dAkmu~R_GqCwDgBgI}DsNaH',
        NextPtr: 821,
        Line: '9>'
    },
    {
        Node_ID: 821,
        lat: 11.46666,
        lng: 104.814188,
        Path: 'md}dAidv~R{Aq@}DqA{Co@eDW{Fa@wQuA',
        NextPtr: 822,
        Line: '9>'
    },
    {
        Node_ID: 822,
        lat: 11.477767,
        lng: 104.815627,
        Path: 'wq~dAmnv~Reg@qDmZ_CwAQ',
        NextPtr: 823,
        Line: '9>'
    },
    {
        Node_ID: 823,
        lat: 11.481721,
        lng: 104.816148,
        Path: 'gw`eAexv~RwHm@cFe@_Ga@',
        NextPtr: 824,
        Line: '9>'
    },
    {
        Node_ID: 824,
        lat: 11.487326,
        lng: 104.816885,
        Path: 'cpaeA{{v~R{DWiGe@yGe@wJo@',
        NextPtr: 825,
        Line: '9>'
    },
    {
        Node_ID: 825,
        lat: 11.495514,
        lng: 104.818105,
        Path: '}rbeAo`w~RiF_@qJo@y^kC',
        NextPtr: 826,
        Line: '9>'
    },
    {
        Node_ID: 826,
        lat: 11.499961,
        lng: 104.818648,
        Path: '}rbeAo`w~RiF_@qJo@y^kC',
        NextPtr: 827,
        Line: '9>'
    },
    {
        Node_ID: 827,
        lat: 11.495936,
        lng: 104.817961,
        Path: 'ideeA}jw~RbZpB',
        NextPtr: 828,
        Line: '9<'
    },
    {
        Node_ID: 828,
        lat: 11.481128,
        lng: 104.815945,
        Path: 'eideAkgw~RpHd@tNfAjNbAvRpAnYpBpEX',
        NextPtr: 829,
        Line: '9<'
    },
    {
        Node_ID: 829,
        lat: 11.47742,
        lng: 104.815374,
        Path: 'wkaeA{zv~RxUnB',
        NextPtr: 830,
        Line: '9<'
    },
    {
        Node_ID: 830,
        lat: 11.467548,
        lng: 104.813998,
        Path: 'yt`eAmwv~RrF^pD\\n\\bCfE\\',
        NextPtr: 831,
        Line: '9<'
    },
    {
        Node_ID: 831,
        lat: 11.460291,
        lng: 104.808124,
        Path: 'sc_eAmpv~RfPjAhGd@tJr@dJr@v@HdB`@fBh@tBt@IxBChIo@fIQtA',
        NextPtr: 832,
        Line: '9<'
    },
    {
        Node_ID: 832,
        lat: 11.460011,
        lng: 104.802539,
        Path: 'qi}dAgju~ROpCH|Cp@`GZpEDxBGhEIh@',
        NextPtr: 833,
        Line: '9<'
    },
    {
        Node_ID: 833,
        lat: 11.467104,
        lng: 104.792524,
        Path: 'wg}dAeft~RaAzHw@zE_AtDyAvDaB`F{BpGq@~Am@v@yAp@cQnFu@^eBnB',
        NextPtr: 834,
        Line: '9<'
    },
    {
        Node_ID: 834,
        lat: 11.468377,
        lng: 104.783638,
        Path: 'ct~dAahr~Ry@|@sAhAq@x@Sj@Mh@YjBKzF?rE[hDA|CRdBrAvH`@|D}AE',
        NextPtr: 835,
        Line: '9<'
    },
    {
        Node_ID: 835,
        lat: 11.472425,
        lng: 104.783869,
        Path: 'a|~dAopp~R{XM',
        NextPtr: 836,
        Line: '9<'
    },
    {
        Node_ID: 836,
        lat: 11.4802,
        lng: 104.783771,
        Path: 'av_eA}pp~R_l@DaB?',
        NextPtr: 837,
        Line: '9<'
    },
    {
        Node_ID: 837,
        lat: 11.484889,
        lng: 104.782977,
        Path: 'qeaeAypp~ReEL}DZeQnB',
        NextPtr: 838,
        Line: '9<'
    },
    {
        Node_ID: 838,
        lat: 11.489445,
        lng: 104.782189,
        Path: 'wcbeAalp~RwLrAuKnA',
        NextPtr: 839,
        Line: '9<'
    },
    {
        Node_ID: 839,
        lat: 11.499959,
        lng: 104.78039,
        Path: 'g~beA{fp~R{\\zDuMnAqUzA',
        NextPtr: 840,
        Line: '9<'
    },
    {
        Node_ID: 840,
        lat: 11.504487,
        lng: 104.779714,
        Path: 'iaeeAu{o~Re[vB',
        NextPtr: 841,
        Line: '9<'
    },
    {
        Node_ID: 841,
        lat: 11.513875,
        lng: 104.77967,
        Path: 'u}eeAuwo~RgPbA}AOyA_BqBN{@hAm^_@',
        NextPtr: 842,
        Line: '9<'
    },
    {
        Node_ID: 842,
        lat: 11.519034,
        lng: 104.779294,
        Path: 'wygeAgwo~RqR]iIxA',
        NextPtr: null,
        Line: '9<'
    },
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    {
        Node_ID: 900,
        lat: 11.525656,
        lng: 104.921075,
        Path: '',
        NextPtr: 901,
        Line: '10>'
    },
    {
        Node_ID: 901,
        lat: 11.519272,
        lng: 104.913879,
        Path: 'kbjeAwkk_SnUx@hB^nCrArCzDnEzPBxGs@~E',
        NextPtr: 902,
        Line: '10>'
    },
    {
        Node_ID: 902,
        lat: 11.519748,
        lng: 104.911402,
        Path: 'mzheAw~i_S_BnN',
        NextPtr: 903,
        Line: '10>'
    },
    {
        Node_ID: 903,
        lat: 11.52202,
        lng: 104.9023,
        Path: 'm}heAgoi_SuEn^oFjX',
        NextPtr: 904,
        Line: '10>'
    },
    {
        Node_ID: 904,
        lat: 11.522801,
        lng: 104.899732,
        Path: 'skieAkvg_S{C`O',
        NextPtr: 905,
        Line: '10>'
    },
    {
        Node_ID: 905,
        lat: 11.52374,
        lng: 104.896509,
        Path: 'evieAcrf_S|DeS',
        NextPtr: 906,
        Line: '10>'
    },
    {
        Node_ID: 906,
        lat: 11.52574,
        lng: 104.888964,
        Path: 'kvieAerf_S_Hz^{@dEi@xG',
        NextPtr: 907,
        Line: '10>'
    },
    {
        Node_ID: 907,
        lat: 11.534788,
        lng: 104.884794,
        Path: 'ubjeAace_SKrF{t@fJgFMtChD@jA',
        NextPtr: 908,
        Line: '10>'
    },
    {
        Node_ID: 908,
        lat: 11.536017,
        lng: 104.883082,
        Path: 'm{keA}hd_St@zIiH@',
        NextPtr: 909,
        Line: '10>'
    },
    {
        Node_ID: 909,
        lat: 11.540332,
        lng: 104.883299,
        Path: 'acleA}}c_S_Zi@',
        NextPtr: 910,
        Line: '10>'
    },
    {
        Node_ID: 910,
        lat: 11.549991,
        lng: 104.885904,
        Path: 'a~leAg_d_Sqg@cAc@mAWkCcBgCmDJ[]OaBe@YyE^',
        NextPtr: 911,
        Line: '10>'
    },
    {
        Node_ID: 911,
        lat: 11.553085,
        lng: 104.883709,
        Path: 'mzneA{od_ScSpBXbI',
        NextPtr: 912,
        Line: '10>'
    },
    {
        Node_ID: 912,
        lat: 11.552773,
        lng: 104.880636,
        Path: 'wmoeAebd_S|@dR',
        NextPtr: 913,
        Line: '10>'
    },
    {
        Node_ID: 913,
        lat: 11.552487,
        lng: 104.877407,
        Path: 'ykoeA_oc_Sv@dS',
        NextPtr: 914,
        Line: '10>'
    },
    {
        Node_ID: 914,
        lat: 11.551326,
        lng: 104.867108,
        Path: 'wioeA{zb_SbFj_A',
        NextPtr: 915,
        Line: '10>'
    },
    {
        Node_ID: 915,
        lat: 11.551212,
        lng: 104.857933,
        Path: 'sboeAoz`_SdElw@sD^',
        NextPtr: 916,
        Line: '10>'
    },
    {
        Node_ID: 916,
        lat: 11.556777,
        lng: 104.85702,
        Path: '_boeAw`__Sga@|D',
        NextPtr: 917,
        Line: '10>'
    },
    {
        Node_ID: 917,
        lat: 11.560712,
        lng: 104.853894,
        Path: 'gdpeAyz~~ReJv@e@R_EhD_@j@yD|H',
        NextPtr: null,
        Line: '10>'
    },
    {
        Node_ID: 918,
        lat: 11.551287,
        lng: 104.857808,
        Path: 'ifpeAcz~~Rvb@eE',
        NextPtr: 919,
        Line: '10<'
    },
    {
        Node_ID: 919,
        lat: 11.550376,
        lng: 104.860898,
        Path: 'qboeAi`__SlEs@o@oP',
        NextPtr: 920,
        Line: '10<'
    },
    {
        Node_ID: 920,
        lat: 11.551632,
        lng: 104.872231,
        Path: 's}neAms__SgF}eA',
        NextPtr: 922,
        Line: '10<'
    },
    null,
    {
        Node_ID: 922,
        lat: 11.552609,
        lng: 104.881005,
        Path: '{doeAkza_SeE{u@',
        NextPtr: 923,
        Line: '10<'
    },
    {
        Node_ID: 923,
        lat: 11.552924,
        lng: 104.884474,
        Path: 'akoeAgqc_S{@uT',
        NextPtr: 924,
        Line: '10<'
    },
    {
        Node_ID: 924,
        lat: 11.549762,
        lng: 104.885861,
        Path: '}loeA}fd_SYcDvSqB',
        NextPtr: 925,
        Line: '10<'
    },
    {
        Node_ID: 925,
        lat: 11.547122,
        lng: 104.884222,
        Path: '_yneAsod_S|Da@\\lChAN~BIpAnBVdB',
        NextPtr: 926,
        Line: '10<'
    },
    {
        Node_ID: 926,
        lat: 11.53946,
        lng: 104.883075,
        Path: 'ohneAked_Sb@vB~@Xvk@jA',
        NextPtr: 927,
        Line: '10<'
    },
    {
        Node_ID: 927,
        lat: 11.534935,
        lng: 104.882849,
        Path: 'qxleAo~c_S`[`@',
        NextPtr: 928,
        Line: '10<'
    },
    {
        Node_ID: 928,
        lat: 11.534224,
        lng: 104.88377,
        Path: 'o|keAm}c_SzBIS{C',
        NextPtr: 929,
        Line: '10<'
    },
    {
        Node_ID: 929,
        lat: 11.525602,
        lng: 104.888898,
        Path: 'gykeAsbd_Sw@cIt@_Bhu@yIZaG',
        NextPtr: 930,
        Line: '10<'
    },
    {
        Node_ID: 930,
        lat: 11.524363,
        lng: 104.893657,
        Path: '_bjeAsbe_SXmGlE_T',
        NextPtr: 931,
        Line: '10<'
    },
    {
        Node_ID: 931,
        lat: 11.523657,
        lng: 104.896337,
        Path: 'wzieAa`f_SzCaP',
        NextPtr: 932,
        Line: '10<'
    },
    {
        Node_ID: 932,
        lat: 11.52251,
        lng: 104.90012,
        Path: '{uieAcqf_S~EwV',
        NextPtr: 933,
        Line: '10<'
    },
    {
        Node_ID: 933,
        lat: 11.521851,
        lng: 104.902519,
        Path: '{nieA{hg_ShC{M',
        NextPtr: 934,
        Line: '10<'
    },
    {
        Node_ID: 934,
        lat: 11.520931,
        lng: 104.90559,
        Path: 'qjieAwwg_SvDeR',
        NextPtr: 935,
        Line: '10<'
    },
    {
        Node_ID: 935,
        lat: 11.51889,
        lng: 104.915846,
        Path: 'ydieA}jh_SlEwZzDs]LwD',
        NextPtr: 936,
        Line: '10<'
    },
    {
        Node_ID: 936,
        lat: 11.522517,
        lng: 104.92101,
        Path: 'axheAakj_Si@gEwCaLuBmEkDoCqFw@',
        NextPtr: null,
        Line: '10<'
    },
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    {
        Node_ID: 1000,
        lat: 11.507161,
        lng: 104.888097,
        Path: '',
        NextPtr: 1001,
        Line: '11>'
    },
    {
        Node_ID: 1001,
        lat: 11.505242,
        lng: 104.884349,
        Path: 'wnfeAs}d_S~JlV',
        NextPtr: 1002,
        Line: '11>'
    },
    {
        Node_ID: 1002,
        lat: 11.500182,
        lng: 104.87443,
        Path: 'wbfeAefd_Sz^~|@',
        NextPtr: 1003,
        Line: '11>'
    },
    {
        Node_ID: 1003,
        lat: 11.496802,
        lng: 104.868223,
        Path: '{beeAehb_SzSxe@',
        NextPtr: 1004,
        Line: '11>'
    },
    {
        Node_ID: 1004,
        lat: 11.49242,
        lng: 104.859945,
        Path: '_ndeAkaa_SjZvr@',
        NextPtr: 1005,
        Line: '11>'
    },
    {
        Node_ID: 1005,
        lat: 11.490713,
        lng: 104.856852,
        Path: 'orceA{m__SpIpR',
        NextPtr: 1006,
        Line: '11>'
    },
    {
        Node_ID: 1006,
        lat: 11.485924,
        lng: 104.849598,
        Path: '}gceAiz~~RvT~e@dGhE',
        NextPtr: 1007,
        Line: '11>'
    },
    {
        Node_ID: 1007,
        lat: 11.476948,
        lng: 104.844943,
        Path: '_jbeA_m}~Rp^nTtWlF',
        NextPtr: 1008,
        Line: '11>'
    },
    {
        Node_ID: 1008,
        lat: 11.468916,
        lng: 104.844407,
        Path: 'wq`eAap|~RtG~@hh@Z',
        NextPtr: 1009,
        Line: '11>'
    },
    {
        Node_ID: 1009,
        lat: 11.464049,
        lng: 104.837933,
        Path: 'w__eAem|~RxA@hYtd@h@jB',
        NextPtr: 1010,
        Line: '11>'
    },
    {
        Node_ID: 1010,
        lat: 11.463128,
        lng: 104.834457,
        Path: 'ca~dAed{~RnCfL',
        NextPtr: 1011,
        Line: '11>'
    },
    {
        Node_ID: 1011,
        lat: 11.462739,
        lng: 104.826357,
        Path: 's|}dA}vz~Rf@~GlCbKfBlOrBpLMlAs@lA{EnB',
        NextPtr: 1012,
        Line: '11>'
    },
    {
        Node_ID: 1012,
        lat: 11.464747,
        lng: 104.820849,
        Path: '}x}dAm{x~RiGhCUh@J~RIp@oB~E',
        NextPtr: 1013,
        Line: '11>'
    },
    {
        Node_ID: 1013,
        lat: 11.46426,
        lng: 104.816053,
        Path: 'oe~dAcyw~RkEdL@hC|@`BnFzG',
        NextPtr: null,
        Line: '11>'
    },
    {
        Node_ID: 1014,
        lat: 11.46219,
        lng: 104.81403,
        Path: '',
        NextPtr: 1015,
        Line: '11<'
    },
    {
        Node_ID: 1015,
        lat: 11.464428,
        lng: 104.816539,
        Path: 'gv}dAknv~RqLwN',
        NextPtr: 1016,
        Line: '11<'
    },
    {
        Node_ID: 1016,
        lat: 11.464729,
        lng: 104.820542,
        Path: 'yc~dAc~v~RkF{GMyChD{I',
        NextPtr: 1017,
        Line: '11<'
    },
    {
        Node_ID: 1017,
        lat: 11.462908,
        lng: 104.8261,
        Path: 'we~dAoww~RhCgH@_UdFkB',
        NextPtr: 1018,
        Line: '11<'
    },
    {
        Node_ID: 1018,
        lat: 11.462875,
        lng: 104.834082,
        Path: 'ez}dAczx~RdGmCt@_AP_B_EsWq@kGuAmF',
        NextPtr: 1019,
        Line: '11<'
    },
    {
        Node_ID: 1019,
        lat: 11.464588,
        lng: 104.839068,
        Path: '',
        NextPtr: 1020,
        Line: '11<'
    },
    {
        Node_ID: 1020,
        lat: 11.464588,
        lng: 104.839068,
        Path: '_z}dA_lz~Rq@{C_@wFoCoKsBaE',
        NextPtr: 1021,
        Line: '11<'
    },
    {
        Node_ID: 1021,
        lat: 11.468161,
        lng: 104.844239,
        Path: 'ud~dAek{~RqU}^',
        NextPtr: 1022,
        Line: '11<'
    },
    {
        Node_ID: 1022,
        lat: 11.477166,
        lng: 104.845166,
        Path: 'g{~dAck|~Ry@kAil@a@}GwA',
        NextPtr: 1023,
        Line: '11<'
    },
    {
        Node_ID: 1023,
        lat: 11.486015,
        lng: 104.849758,
        Path: 'is`eAiq|~RmSsD}Ai@_BeA}[qR',
        NextPtr: 1024,
        Line: '11<'
    },
    {
        Node_ID: 1024,
        lat: 11.489894,
        lng: 104.855609,
        Path: 'sjbeA_n}~RyFuD_ByDkLaX',
        NextPtr: 1025,
        Line: '11<'
    },
    {
        Node_ID: 1025,
        lat: 11.492188,
        lng: 104.859822,
        Path: 'ybceAqr~~RkMiY',
        NextPtr: 1026,
        Line: '11<'
    },
    {
        Node_ID: 1026,
        lat: 11.496387,
        lng: 104.867718,
        Path: 'eqceA{l__SqYcp@',
        NextPtr: 1027,
        Line: '11<'
    },
    {
        Node_ID: 1027,
        lat: 11.499329,
        lng: 104.873233,
        Path: 'wkdeA_~`_SaQua@',
        NextPtr: 1028,
        Line: '11<'
    },
    {
        Node_ID: 1028,
        lat: 11.504347,
        lng: 104.882916,
        Path: 'y}deAu`b_Su^g{@',
        NextPtr: 1029,
        Line: '11<'
    },
    {
        Node_ID: 1029,
        lat: 11.507818,
        lng: 104.889709,
        Path: 'o}eeA}|c_S{Tki@',
        NextPtr: null,
        Line: '11<'
    },
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    {
        Node_ID: 1200,
        lat: 11.533247,
        lng: 104.914305,
        Path: '',
        NextPtr: 1201,
        Line: '13>'
    },
    {
        Node_ID: 1201,
        lat: 11.533247,
        lng: 104.914305,
        Path: '{qkeAiaj_SeHF',
        NextPtr: 1202,
        Line: '13>'
    },
    {
        Node_ID: 1202,
        lat: 11.536905,
        lng: 104.914229,
        Path: '_{keAcaj_SsLH',
        NextPtr: 1203,
        Line: '13>'
    },
    {
        Node_ID: 1203,
        lat: 11.539731,
        lng: 104.914162,
        Path: 'uhleAy`j_SqPP',
        NextPtr: 1204,
        Line: '13>'
    },
    {
        Node_ID: 1204,
        lat: 11.545314,
        lng: 104.914017,
        Path: 'ezleAg`j_SaUR{KF',
        NextPtr: 1205,
        Line: '13>'
    },
    {
        Node_ID: 1205,
        lat: 11.547633,
        lng: 104.913956,
        Path: 'e}meAk_j_SeJHkB@',
        NextPtr: 1206,
        Line: '13>'
    },
    {
        Node_ID: 1206,
        lat: 11.549498,
        lng: 104.913888,
        Path: '{kneA__j_SuJJ',
        NextPtr: 1207,
        Line: '13>'
    },
    {
        Node_ID: 1207,
        lat: 11.552988,
        lng: 104.913796,
        Path: 'qwneAs~i_SqTP',
        NextPtr: 1208,
        Line: '13>'
    },
    {
        Node_ID: 1208,
        lat: 11.554782,
        lng: 104.914073,
        Path: 'emoeAa~i_SwBBsFw@',
        NextPtr: 1209,
        Line: '13>'
    },
    {
        Node_ID: 1209,
        lat: 11.554782,
        lng: 104.914073,
        Path: 'emoeAa~i_SwBBsFw@',
        NextPtr: null,
        Line: '13>'
    },
    {
        Node_ID: 1210,
        lat: 11.554781,
        lng: 104.913864,
        Path: '',
        NextPtr: 1211,
        Line: '13<'
    },
    {
        Node_ID: 1211,
        lat: 11.553047,
        lng: 104.913661,
        Path: 'gxoeAe_j_StEr@fC@',
        NextPtr: 1212,
        Line: '13<'
    },
    {
        Node_ID: 1212,
        lat: 11.550089,
        lng: 104.913769,
        Path: 'mmoeAq}i_SlQO',
        NextPtr: 1213,
        Line: '13<'
    },
    {
        Node_ID: 1213,
        lat: 11.54861,
        lng: 104.913804,
        Path: '_{neAa~i_SbHG',
        NextPtr: 1214,
        Line: '13<'
    },
    {
        Node_ID: 1214,
        lat: 11.536369,
        lng: 104.914124,
        Path: '{qneAi~i_SrUUbEEzGCbROdOOxBA',
        NextPtr: 1215,
        Line: '13<'
    },
    {
        Node_ID: 1215,
        lat: 11.534605,
        lng: 104.914158,
        Path: 'celeAk`j_SxIG',
        NextPtr: 1216,
        Line: '13<'
    },
    {
        Node_ID: 1216,
        lat: 11.533069,
        lng: 104.914175,
        Path: 'izkeAs`j_StHE',
        NextPtr: null,
        Line: '13<'
    }
]