import { GeoDif } from './geo-dif'
import DB from './db'
export async function InRange(User) {
    var MaxInrange = 16
    let AllNodeCloseToUser = [], NumNodeCloseToUser = []
    // console.log('Finding Closest Bus Stop To Get On____________');
    // + find node within range(one node per b line)
    // pushing the first one, may be repalced later
    AllNodeCloseToUser.push({
        Line: DB[0].Line,
        Node_ID: DB[0].Node_ID,
        Distance: await GeoDif(DB[0].lat, DB[0].lng, User.lat, User.lng)
    })
    // console.log(' Starting to Comparing and Finding Closest....');
    // 	+ scan through all node
    for (let i = 1; i < DB.length; i++) {
        if (DB[i] === null) continue;
        // console.log('Comparing with Node ID', DB[i].Node_ID)
        // 		if(Closest Node) close than every other node -- if smaller than other node increment count then when count == total node return this drop off location
        let node_of_line_exist = false;
        //TODO store this somewhere
        let new_node_distance = await GeoDif(DB[i].lat, DB[i].lng, User.lat, User.lng);
        // check with pushed nodes
        for (let j = 0; j < AllNodeCloseToUser.length; j++) {
            // console.log('Seeing if this Node is closer to the user than previous selected');
            if (DB[i].Line == AllNodeCloseToUser[j].Line) {
                // console.log('Line Match')
                node_of_line_exist = true;
                if (new_node_distance <= AllNodeCloseToUser[j].Distance) {
                    // console.log('Distance Smaller, replacing')
                    AllNodeCloseToUser[j] = {
                        Line: DB[i].Line,
                        Node_ID: DB[i].Node_ID,
                        Distance: await GeoDif(DB[i].lat, DB[i].lng, User.lat, User.lng)
                    };
                } else {
                    // console.log('Line Match But Further')
                }
            } else {
                // console.log('Line Not Match')
            }
        }

        if (!node_of_line_exist) {
            // TODO:
            // 		+ if(within 1km) {

            // console.log('Line not contained any node, pushing this Node as Closest for the line')
            AllNodeCloseToUser.push({
                Line: DB[i].Line,
                Node_ID: DB[i].Node_ID,
                Distance: await GeoDif(DB[i].lat, DB[i].lng, User.lat, User.lng)
            });
        }
        // console.log('End of Inner Loop Result:', JSON.stringify(AllNodeCloseToUser))
    }
    // console.log('RESULT ::: COMPARING TO ALL NODE DONE. Closest Nodes, one of each line: ', JSON.stringify(AllNodeCloseToUser));
    // console.log('FINDING CLOSEST 3__________________')
    // assuming first 3 is the closest
    
    for (let i = 0; i< MaxInrange; i++){
        NumNodeCloseToUser.push(
            DB[AllNodeCloseToUser[i].Node_ID]
        );
        NumNodeCloseToUser[i].Distance = AllNodeCloseToUser[i].Distance;
    }
    for (let i = MaxInrange; i < AllNodeCloseToUser.length; i++) {
        // console.log('Stepping through each Node')
        let replaced = false;
        for (let j = 0; j < MaxInrange; j++) {
            if (replaced) continue;
            // console.log('lopping through best of three')
            // console.log('Compare ', JSON.stringify(AllNodeCloseToUser[i]), 'With 3 Bester', JSON.stringify(NumNodeCloseToUser[j]))
            if (AllNodeCloseToUser[i].Distance < NumNodeCloseToUser[j].Distance) {
                // console.log('Replacing best of three')
                NumNodeCloseToUser[j] = DB[AllNodeCloseToUser[i].Node_ID];
                NumNodeCloseToUser[j].Distance = AllNodeCloseToUser[i].Distance;
                replaced = true;
            }
        }
    }
    NumNodeCloseToUser = await NumNodeCloseToUser.sort(function(a, b){
        return a.Distance - b.Distance
    })
    //  this.ThreeNodeCloseToDestination.forEach(node => {
    //   NumNodeCloseToUser.push(node);
    // })
    // console.log('RESULT::: Closest Nodes: _______________________', JSON.stringify(NumNodeCloseToUser, null, 2));
    return NumNodeCloseToUser
}