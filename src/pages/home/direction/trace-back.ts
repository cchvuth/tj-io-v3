import { ShortestPath } from './shortest-path';
import DB from './db'

export async function TraceBack(User, Destination, bias, precision, InRange) {
    // TODO: store initial somewhere sp dpn't have to trace
    let Start_Finish = await ShortestPath(User, Destination, bias, precision, InRange);
    let Route = [];
    Route.push(Start_Finish.OneClosestUser);
    let NextNode = Start_Finish.OneClosestUser;
    while (NextNode.NextPtr != null) {
        if (NextNode.Node_ID == Start_Finish.OneClosestDest.Node_ID) break;
        NextNode = DB[NextNode.NextPtr]
        Route.push(NextNode);
    }
    // let Reversed_Route = [];
    // for (let i = Route.length - 1; i >= 0; i--) {
    //     Reversed_Route.push(Route[i]);
    // }
    return Route;
}