import { TraceBack } from './trace-back'
import { GeoDif } from './geo-dif';
import DB from './db'

export async function Assemble(User, Destination, InRange) {
    // console.log('---------------------------STARTING-----------------------------------')
    let FullRoute = []
    // Search and push result in FullRoute
    FullRoute.push(await TraceBack(User, Destination, 1, 0.004, InRange));

    // Keep searching on other bus line until the Node_ID in the one before latest result array[0] is the same as the latest search result
    while (1) {
        let Counter = 0;
        let is_exit = false
        if (FullRoute[Counter][FullRoute[Counter].length - 1]) {
            console.log('Changing User Location');
            let waypoint = {
                lat: FullRoute[Counter][FullRoute[Counter].length - 1].lat,
                lng: FullRoute[Counter][FullRoute[Counter].length - 1].lng
            }
            Counter++;
            let traced = await TraceBack(waypoint, Destination, 1, 0.001, null)
            // check dubplicate
            for (let i = FullRoute[FullRoute.length - 1].length - 1; i >= 0; i--) {
                for (let j = traced.length - 1; j >= 0; j--) {
                    if (FullRoute[FullRoute.length - 1][i].Node_ID === traced[j].Node_ID) {
                        traced = []
                        is_exit = true;
                        break;
                    }
                }
                if (is_exit) break;
            }
            if (is_exit) break;
            await FullRoute.push(traced)
        } else {
            break;
        }
    }

    console.log(FullRoute.length);
    

   await CleanSingles(FullRoute)

    console.log('Add trying from user again');
    while (1) {
        let is_exit = false
        if (FullRoute[0] && FullRoute[0][0]) {
            console.log('Changing Destination Location');
            let waypoint = {
                lat: FullRoute[0][0].lat,
                lng: FullRoute[0][0].lng
            }
            let traced = await TraceBack(User, waypoint, 2, 0.004, null)
            for (let i = 0; i < FullRoute[0].length; i++) {
                for (let j = 0; j < traced.length; j++) {
                    if (FullRoute[0][i].Node_ID === traced[j].Node_ID) {
                        is_exit = true;
                        traced = []
                        break;
                    }
                }
                if (is_exit) break;
            }
            if (is_exit) break;
            await FullRoute.unshift(traced)
        } else {
            break;
        }
    }
    await CleanSingles(FullRoute)

    // console.log('Clean up start');
    // if (FullRoute[0]) {
    //     let distanceUserDestination = await GeoDif(User.lat, User.lng, Destination.lat, Destination.lng)
    //     for (let i = 0; i < FullRoute[0].length; i++) {
    //         if (
    //             await GeoDif(FullRoute[0][i].lat, FullRoute[0][i].lng, Destination.lat, Destination.lng)
    //             >
    //             distanceUserDestination
    //         ) {
    //             await FullRoute[0].splice(0, 1)
    //         } else {
    //             break;
    //         }
    //     }
    //     await CleanSingles(FullRoute)
    // }

    console.log('Cleaning end');
    if (FullRoute[0] && FullRoute[0][0] && (FullRoute[0].length > 1)) {
        for (let i = FullRoute[FullRoute.length - 1].length - 1; i > 0; i--) {
            if (
                await GeoDif(FullRoute[FullRoute.length - 1][i].lat, FullRoute[FullRoute.length - 1][i].lng, Destination.lat, Destination.lng)
                >
                await GeoDif(FullRoute[FullRoute.length - 1][i - 1].lat, FullRoute[FullRoute.length - 1][i - 1].lng, Destination.lat, Destination.lng)
            ) {
                await FullRoute[FullRoute.length - 1].splice(i, 1)
            } else {
                break;
            }
        }
        await CleanSingles(FullRoute)
    }

    
    console.log('Clean up connecting routes');
    if (FullRoute.length > 1) {
        for (let i = 0; i < FullRoute.length - 1; i++) { // need to stop before the last index
            for (let j = FullRoute[i].length - 1; j > 0; j--) {
                if ((FullRoute[i][j] || FullRoute[i+1][j]) && FullRoute[i][j - 1]) {
                    if ( // test current, remove current
                        GeoDif(FullRoute[i][j].lat, FullRoute[i][j].lng, FullRoute[i + 1][0].lat, FullRoute[i + 1][0].lng)
                        >
                        GeoDif(FullRoute[i][j - 1].lat, FullRoute[i][j - 1].lng, FullRoute[i + 1][0].lat, FullRoute[i + 1][0].lng)
                    ) {
                        console.log('DEL', i,j);
                        FullRoute[i].splice(j, 1)
                    } else if ( // test next i+1, remove i+1 
                        GeoDif(FullRoute[i][j].lat, FullRoute[i][j].lng, FullRoute[i + 1][0].lat, FullRoute[i + 1][0].lng)
                        >
                        GeoDif(FullRoute[i][j].lat, FullRoute[i][j].lng, FullRoute[i + 1][1].lat, FullRoute[i + 1][1].lng)
                    ) {
                        console.log('DEL', i,j);
                        FullRoute[i + 1].splice(0, 1)
                    } else {
                        break;
                    }
                }
            }
        }
        await CleanSingles(FullRoute)
    }

    // Print result
    for (let i = 0; i < FullRoute.length; i++) {
        if (FullRoute[i]) {
            for (let j = 0; j < FullRoute[i].length; j++) {
                console.log("Full Path:", i, JSON.stringify(FullRoute[i][j].Node_ID));
            }
        }
    }
    return FullRoute
}

function CleanSingles(FullRoute) {
    console.log('Clean up singles');
    for (let i = 0; i < FullRoute.length; i++) {
        if (FullRoute[i]&& (FullRoute[i].length === 1)) {
            FullRoute.splice(i, 1)
            i--;
        }
    }
}