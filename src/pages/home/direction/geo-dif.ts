export function GeoDif(lat1, lon1, lat2, lon2) {
    return Math.abs(lat2 - lat1) + Math.abs(lon2 - lon1);
}