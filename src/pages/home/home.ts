import { Component, ChangeDetectorRef } from '@angular/core'
import { NavController, Events, Platform, LoadingController } from 'ionic-angular'
import { HTTP } from '@ionic-native/http'
import { LocationAccuracy } from '@ionic-native/location-accuracy'
import { Geolocation, Geoposition } from '@ionic-native/geolocation';

import { DataSharingService } from './services/data-sharing'

import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  GoogleMapsAnimation,
  Environment,
  Polyline,
  MyLocation,
  ILatLng,
  LatLng,
  LocationService,
  GeocoderRequest,
  Geocoder,
  GeocoderResult,
  Marker,
  MarkerCluster
} from '@ionic-native/google-maps'
import lines from './data/lines-new'
import MapStyle from './map-style'

import { InsertPolyline } from './helper/polyline'
import { AddMarkers } from './helper/marker'
import { CreatePath } from './helper/trip-path'

import { Assemble } from './direction/assembler'
import { InRange } from './direction/in-range'
import { AddAllStops } from './helper/add_all_stops'
import { GeoDif } from './direction/geo-dif';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  test_mode = false
  google_key = 'AIzaSyDs6Ql9vlvXnyBAEJbWCddWZchY35aq_38'
  here_key = {
    app_code: '7wLojSFiIBfHXd4ACJ3YYA',
    app_id: '78WiIhEj7tlyv4nGvxPg',
  }
  graphhopper_key = '25458c15-c3b6-44c8-b032-5565ed0309a3'
  walk_line_color = '#ff9700'
  map: GoogleMap

  searchInput: string
  searchResult: string[] = []

  watcher: any
  userLocation: ILatLng = new LatLng(11.568357, 104.900066) // Default assumption
  userMarker: Marker
  userDestination: ILatLng = new LatLng(11.556161, 104.920364) // Default assumption
  closeByBusStops = []

  tripMarkers: Marker[] = []
  tripPolylines: Polyline[] = []
  fullRoute: any = []
  nearbyLineCache = null
  allStops: MarkerCluster
  allBLines: Polyline[] = []

  is_searching: boolean = false
  is_marker_selected: boolean = false
  is_en_route: boolean = false
  show_alert: boolean = false
  search_in_progress: boolean = false

  near_dr = 0.0003
  mid_dr = 0.0015
  far_dr = 0.004

  lock: boolean = false
  log_text: any

  constructor(
    public navCtrl: NavController,
    public events: Events,
    private http: HTTP,
    platform: Platform,
    private changeDetectorRef: ChangeDetectorRef,
    public sharedData: DataSharingService,
    private locationAccuracy: LocationAccuracy,
    private geolocation: Geolocation,
    public loadingCtrl: LoadingController
  ) {
    events.subscribe('go:nearest', () => {
      this.GoNearest()
    })
    events.subscribe('go:marker', () => {
      this.Go(null)
    })
    events.subscribe('nav:main', () => {
      this.BackToMain()
    })
    events.subscribe('alert:close', () => {
      this.show_alert = false
      this.changeDetectorRef.detectChanges()
    })
    events.subscribe('toggle:all-stops', () => {
      this.ToggleAllStops()
    })
    events.subscribe('toggle:all-b-lines', () => {
      this.ToggleAllPolyline()
    })
    platform.registerBackButtonAction(() => {
      this.BackToMain()
    }, 2)
    window.addEventListener('keyboardWillHide', () => {
      this.is_searching = false
      this.changeDetectorRef.detectChanges()
    });
  }

  async ngOnInit() {
    this.loadMap()
  }

  async loadMap() {
    Environment.setEnv({
      'API_KEY_FOR_BROWSER_RELEASE': this.google_key,
      'API_KEY_FOR_BROWSER_DEBUG': this.google_key
    });
    let element: HTMLElement = document.getElementById('map_canvas');
    let options: GoogleMapOptions = {
      controls: {
        //'compass': true,
        myLocation: true,   // (blue dot)
        zoom: true,   
        mapToolbar: false       // android only
      },
      gestures: {
        tilt: false
      },
      camera: {
        target: { lat: this.userLocation.lat, lng: this.userLocation.lng }
      },
      preferences: {
        zoom: {
          minZoom: 11,
          maxZoom: 18
        },
        building: true,
        padding: {
          left: 10,
          top: 100,
          bottom: 120,
          right: 10
        }
      },
      ...MapStyle
    };
    this.map = GoogleMaps.create(element, options)

    // LINES
    for (let i = 0; i < lines.length; i++) {
      this.allBLines.push(await InsertPolyline(this.map, lines[i].code, lines[i].color, lines[i].width, lines[i].index))
    }
    // BUS STOPS AND ADD EVENT LISTENER
    this.allStops = await AddAllStops(this.map)
    this.AddBusStopEvent(this.allStops)

    this.map.on(GoogleMapsEvent.MAP_CLICK).subscribe(() => {
      if (this.is_marker_selected) this.BackToMain()
    })

    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (canRequest) {
        // the accuracy option will be ignored by iOS
        this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
          async () => {
            await LocationService.getMyLocation().then(async (myLocation: MyLocation) => {
              this.log(myLocation)
              this.map.animateCamera({
                target: { lat: myLocation.latLng.lat, lng: myLocation.latLng.lng },
                duration: 1000,
                zoom: 15
              })
              this.userLocation.lat = myLocation.latLng.lat
              this.userLocation.lng = myLocation.latLng.lng

              // ME
              // this.userMarker = await AddMarkers(this.map, 'me.gif', this.userLocation, 0, 40, 40)
              this.closeByBusStops = await InRange(this.userLocation)
            });

            // NATIVE
            // await this.geolocation.getCurrentPosition({ timeout: 5000 }).then(resp => {
            //   // resp.coords.latitude
            //   // resp.coords.longitude
            //   this.map.animateCamera({
            //     target: { lat: resp.coords.latitude, lng: resp.coords.longitude },
            //     duration: 1000,
            //     zoom: 15
            //   })
            //   this.userLocation.lat = resp.coords.latitude
            //   this.userLocation.lng = resp.coords.longitude
            //  }).catch((error) => {
            //    console.log('location error');
            //  });

            // GET 3 CLOSEBY STOPS
          },
          error => console.log('Error requesting location permissions', error)
        );
      }
    })

    // this.map.addTileOverlay({
    //   getTile: (x: number, y: number, z: number) => {
    //     return `https://tiles.wmflabs.org/osm-no-labels/${z}/${x}/${y}.png`;
    //   },
    //   visible: true,
    //   zIndex: 0,
    //   tileSize: 512,
    //   opacity: 1.0,
    //   debug: false
    // });

    this.watcher = this.geolocation.watchPosition({
      timeout: 60000,
      enableHighAccuracy: true
    })
  }

  async GoNearest() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.userDestination = {
      lat: this.closeByBusStops[0].lat,
      lng: this.closeByBusStops[0].lng
    }
    this.watcher.subscribe((data: Geoposition) => {
      if (data.coords) {
        this.userLocation = {
          lat: data.coords.latitude,
          lng: data.coords.longitude
        }
        // this.userMarker.setPosition(this.userLocation)
        if (GeoDif(
          this.userDestination.lat,
          this.userDestination.lng,
          this.userLocation.lat,
          this.userLocation.lng)
          < this.near_dr
        ) {
          this.is_marker_selected = false
          this.is_en_route = false
          this.show_alert = true
          this.sharedData.messageChanges({
            title: 'You have reached the bus stop.',
            body: 'A bus will arrive in around 10-15 minutes.'
          })
          this.watcher.unsubscribe()
          this.changeDetectorRef.detectChanges()
        }
      }
    });
    this.sharedData.tripDetailChanges({
      id: '',
      line: this.closeByBusStops[0].Line,
      stop_number: this.closeByBusStops[0].Node_ID,
      summary: 'Go to the first stop',
      mode: 'Bus',
      duration: '10mn'
    })
    this.is_marker_selected = false
    this.is_en_route = true
    this.changeDetectorRef.detectChanges()
    this.ClearMap()
    if (!this.lock) {
      this.DirectionRequest(
        { lat: this.userLocation.lat, lng: this.userLocation.lng },
        [
          { lglat: '' }
        ],
        { lat: this.userDestination.lat, lng: this.userDestination.lng },
        this.walk_line_color, '8', '2'
      )
      loading.dismiss();
      this.lock = true
      setTimeout(_ => {
        this.lock = false
      }, 180000) // 5mn
    } else {
      console.log('Sorry Over Limit')
      this.tripPolylines.push(await InsertPolyline(this.map, this.nearbyLineCache, this.walk_line_color, '8', '2'))
      loading.dismiss();
    }
    this.map.animateCamera({
      target: [
        { lat: this.userLocation.lat, lng: this.userLocation.lng },
        { lat: this.closeByBusStops[0].lat, lng: this.closeByBusStops[0].lng }
      ],
      duration: 1000,
      padding: 30
    })
    return await AddMarkers(this.map, 'bus-stop.png', this.userDestination, 0, 26, 34)
  }

  async Go(destination) {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.ClearMap()

    this.tripPolylines = [];
    this.tripMarkers = [];

    if (destination) {
      this.userDestination = {
        lat: destination.lat,
        lng: destination.lng
      }
    }
    
    // Run
    this.fullRoute = await Assemble(this.userLocation, this.userDestination, this.closeByBusStops)

    if (!this.test_mode) {
      if (this.fullRoute[0]) {
        this.DirectionRequest(
          { lat: this.userLocation.lat, lng: this.userLocation.lng },
          [
            { lglat: '' }
          ],
          { lat: this.fullRoute[0][0].lat, lng: this.fullRoute[0][0].lng },
          this.walk_line_color, '8', '2'
        )
      }
      if (this.fullRoute.length === 2) {
        this.DirectionRequest(
          { lat: this.fullRoute[0][this.fullRoute[0].length - 1].lat, lng: this.fullRoute[0][this.fullRoute[0].length - 1].lng },
          [
            { lglat: '' }
          ],
          { lat: this.fullRoute[1][0].lat, lng: this.fullRoute[1][0].lng },
          this.walk_line_color, '8', '2'
        )
      }
      if (destination) {
        this.DirectionRequest(
          { lat: this.fullRoute[this.fullRoute.length - 1][this.fullRoute[this.fullRoute.length - 1].length - 1].lat, lng: this.fullRoute[this.fullRoute.length - 1][this.fullRoute[this.fullRoute.length - 1].length - 1].lng },
          [
            { lglat: '' }
          ],
          { lat: this.userDestination.lat, lng: this.userDestination.lng },
          this.walk_line_color, '8', '2'
        )
      }
    }

    this.tripMarkers.push(await AddMarkers(this.map, 'destination.png', this.userDestination, 1, 26, 34))

    this.map.animateCamera({
      target: [
        { lat: this.userLocation.lat, lng: this.userLocation.lng },
        { lat: this.userDestination.lat, lng: this.userDestination.lng }
      ],
      duration: 1000,
      padding: 60
    })

    this.sharedData.tripDetailChanges({
      id: '',
      line: this.fullRoute[0] ? this.fullRoute[0][0].Line : '',
      stop_number: this.fullRoute[0] ? this.fullRoute[0][0].Node_ID : '',
      summary: 'Go to the first stop',
      mode: 'Bus',
      duration: '10mn'
    })

    this.is_marker_selected = false
    this.is_en_route = true
    this.changeDetectorRef.detectChanges()

    loading.dismiss()

    let { createdPoly, createdMarker } = await CreatePath(this.map, this.fullRoute)

    this.tripPolylines = this.tripPolylines.concat(createdPoly)
    this.tripMarkers = this.tripMarkers.concat(createdMarker)
    // Detect if user had arrived
    var seen1 = false, seen2 = false, seen3 = false
    this.watcher.subscribe((data: Geoposition) => {
      if (data.coords) {
        this.userLocation = {
          lat: data.coords.latitude,
          lng: data.coords.longitude
        }
        // this.userMarker.setPosition(this.userLocation)
        if (!seen1 && GeoDif(
          this.fullRoute[0][0].lat,
          this.fullRoute[0][0].lng,
          this.userLocation.lat,
          this.userLocation.lng)
          < this.near_dr
        ) {
          this.sharedData.messageChanges({
            title: 'You have reached the bus stop.',
            body: 'A bus will arrive in around 10-15 minutes.'
          })
          this.show_alert = true
          seen1 = true
        } else if (!seen2 && GeoDif(
          this.fullRoute[this.fullRoute.length - 1][this.fullRoute[this.fullRoute.length - 1].length - 1].lat,
          this.fullRoute[this.fullRoute.length - 1][this.fullRoute[this.fullRoute.length - 1].length - 1].lng,
          this.userLocation.lat,
          this.userLocation.lng)
          < this.far_dr
        ) {
          this.sharedData.messageChanges({
            title: 'You\'ve almost arrived.',
            body: 'Please drop off at Stop 8A'
          })
          this.show_alert = true
          seen2 = true
        } else if (!seen3 && GeoDif(
          this.userDestination.lat,
          this.userDestination.lng,
          this.userLocation.lat,
          this.userLocation.lng)
          < this.mid_dr
        ) {
          this.sharedData.messageChanges({
            title: 'You have reached your destination',
            body: 'Thank you for using our app!'
          })
          this.is_marker_selected = false
          this.is_en_route = false
          this.show_alert = true
          seen3 = true
          this.watcher.unsubscribe()
        }
        this.changeDetectorRef.detectChanges()
      }
    });
  }

  // get the param, assemble the get request and call draw line function
  DirectionRequest(origin, waypoints, destination, color, width, index) {
    // GRAPHOPPER ##########################################################################################
    let url = `https://graphhopper.com/api/1/route?point=${origin.lat},${origin.lng}&point=${destination.lat},${destination.lng}&locale=kh&vehicle=car&key=${this.graphhopper_key}`;
    this.http.get(url, {}, {})
      .then(async data => {
        let path = JSON.parse(data.data);
        if (path.paths.length !== 0) {
          console.log('USING GRAPHOPPER DIRECTION API')
          this.nearbyLineCache = path.paths[0].points
          return this.tripPolylines.push(await InsertPolyline(this.map, path.paths[0].points, color, width, index))
        } else {
          // HERE ##########################################################################################
          let url = `https://route.api.here.com/routing/7.2/calculateroute.json`;
          this.http.get(url, {
            ...this.here_key,
            waypoint0: `geo!${origin.lat},${origin.lng}`,
            waypoint1: `geo!${destination.lat},${destination.lng}`,
            mode: 'fastest;car;traffic:disabled'
          }, {})
            .then(async data => {
              let path = JSON.parse(data.data);
              if (path.response.route.length !== 0) {
                console.log('USING HERE DIRECTION API')
                let points = []
                path.response.route[0].leg[0].maneuver.forEach(maneuver => {
                  points.push({
                    lat: maneuver.position.latitude,
                    lng: maneuver.position.longitude,
                  })
                })
                this.nearbyLineCache = points
                return this.tripPolylines.push(await InsertPolyline(this.map, points, color, width, index))
              } else {
                // GOOGLE ##########################################################################################
                // let waypointString = waypoints[0].lglat;
                // for (let i = 1; i < waypoints.length; i++) {
                //   waypointString = waypointString + "|" + waypoints[i].lglat;
                // }
                let url = 'https://maps.googleapis.com/maps/api/directions/json?';
                this.http.get(url, {
                  origin: `${origin.lat},${origin.lng}`,
                  // waypoints: waypointString,
                  destination: `${destination.lat},${destination.lng}`,
                  key: this.google_key
                }, {})
                  .then(async data => {
                    let path = JSON.parse(data.data);
                    if (path.routes.length !== 0) {
                      console.log('USING GOOGLE DIRECTION API')
                      this.nearbyLineCache = path.routes[0].overview_polyline.points
                      return this.tripPolylines.push(await InsertPolyline(this.map, path.routes[0].overview_polyline.points, color, width, index))
                    } else {
                      // OSRM ##########################################################################################
                      let url = `http://router.project-osrm.org/route/v1/driving/${origin.lng},${origin.lat};${destination.lng},${destination.lat}`;
                      var satisfied = false
                      while (!satisfied) {
                        console.log('TRYING OSRM DIRECTION API')
                        await this.http.get(url, {
                          overview: 'full'
                        }, {})
                          .then(async data => {
                            let path = JSON.parse(data.data);
                            if (path.routes.length !== 0) {
                              console.log('USING OSRM DIRECTION API')
                              satisfied = true
                              this.nearbyLineCache = path.routes[0].geometry
                              return this.tripPolylines.push(await InsertPolyline(this.map, path.routes[0].geometry, color, width, index))
                            } else {
                              console.log('All direction server exhausted');
                            }
                          })
                          .catch(error => {
                            this.log(error)
                          });
                      }
                    }
                  })
                  .catch(error => {
                    this.log(error)
                  });
              }
            })
            .catch(error => {
              this.log(error)
            });
        }
      })
      .catch(error => {
        this.log(error)
      });
  }

  async AddBusStopEvent(markers: MarkerCluster) {
    await markers.on(GoogleMapsEvent.MARKER_CLICK).subscribe((params) => {
      this.userDestination = params[0]
      let marker = params[1]
      let info = marker.get('info')
      this.sharedData.tripDetailChanges({
        id: info.id,
        line: info.line,
        stop_number: info.stop_number,
        location: info.location,
        summary: info.summary,
        mode: info.mode,
        duration: info.duration
      })
      this.is_marker_selected = true
      this.changeDetectorRef.detectChanges()

      return marker.setAnimation(GoogleMapsAnimation.BOUNCE);
    })
  }

  // search and return matched places
  async GetSearchResult() {
    // // places search (more detailed result)
    // var input = text.replace(/ /g, '+');
    // var location = '&location=' + this.userLocation.lat + ',' + this.userLocation.lng + '&radius=10000';
    // const key = "&key=AIzaSyCgCFzcdwqGrdllTQswN-k-n_C8CIKfaUw";
    // var url = 'https://maps.googleapis.com/maps/api/place/textsearch/json?query=' + input + location + key;

    if (!this.search_in_progress) {
      this.search_in_progress = true
      let input = this.searchInput.replace(/ /g, '+')
      // GOOGLE ##########################################################################################
      let url = 'https://maps.googleapis.com/maps/api/place/autocomplete/json';
      await this.http.get(url, {
        input: input,
        location: `${this.userLocation.lat},${this.userLocation.lng}`,
        radius: '10000',
        key: this.google_key
      }, {})
        .then(async data => {
          let predictions = JSON.parse(data.data)

          if (predictions.predictions.length !== 0) {
            console.log('USING GOOGLE')
            let response = [];
            for (let i = 0; i < predictions.predictions.length; i++) {
              let options = {
                address: predictions.predictions[i].description.split(',')[0] + ', Phnom Penh, Cambodia'
              }
              // Address -> latitude,longitude
              Geocoder.geocode(options)
                .then((results: GeocoderResult[]) => {
                  if (results[0]) {
                    response.push({
                      name: predictions.predictions[i].description,
                      coords: {
                        lat: results[0].position.lat,
                        lng: results[0].position.lng
                      }
                    })
                  } else {
                    console.log('Geocoding Error');
                  }
                })
            }
            this.search_in_progress = false
            return this.searchResult = response;
          } else {
            // GRAPHOPPER ##########################################################################################
            let url = 'https://graphhopper.com/api/1/geocode';
            await this.http.get(url, {
              q: input + ' Phnom Penh',
              point: `${this.userLocation.lat},${this.userLocation.lng}`,
              locale: 'kh',
              key: this.graphhopper_key
            }, {})
              .then(async data => {
                let predictions = JSON.parse(data.data);
                if (predictions.hits.length !== 0) {
                  console.log('USING GRAPHOPPER')
                  let response = [];
                  for (let i = 0; i < predictions.hits.length; i++) {
                    response.push({
                      name: predictions.hits[i].name,
                      coords: {
                        lat: predictions.hits[i].point.lat,
                        lng: predictions.hits[i].point.lng
                      }
                    })
                  }
                  this.search_in_progress = false
                  return this.searchResult = response;
                } else {
                  // HERE ##########################################################################################
                  let url = 'https://places.cit.api.here.com/places/v1/autosuggest'
                  await this.http.get(url, {
                    ...this.here_key,
                    in: `${this.userLocation.lat},${this.userLocation.lng};r=10000`,
                    q: input
                  }, {})
                    .then(async data => {
                      let predictions = JSON.parse(data.data)
                      if (predictions.results.length !== 0) {
                        console.log('USING HERE')
                        let response = [];
                        for (let i = 0; i < predictions.results.length - 1; i++) {
                          response.push({
                            name: predictions.results[i].title,
                            coords: {
                              lat: predictions.results[i].position[0],
                              lng: predictions.results[i].position[1]
                            }
                          })
                        }
                        this.search_in_progress = false
                        return this.searchResult = response;
                      } else {
                        this.search_in_progress = false
                        console.log('All autocomplete server exhausted');
                      }
                    })
                    .catch(error => {
                      this.log(error);
                    });
                }
              })
              .catch(error => {
                this.log(error)
              });
          }
        })
        .catch(error => {
          this.log(error)
        });
    }
    return
  }


  async BackToMain() {
    this.is_searching = false
    this.is_marker_selected = false
    this.is_en_route = false
    this.show_alert = false
    this.changeDetectorRef.detectChanges()

    if (this.tripPolylines.length !== 0) this.tripPolylines.forEach((line: Polyline) => {
      line.destroy()
      this.tripPolylines = []
    })
    if (this.tripMarkers.length !== 0) this.tripMarkers.forEach((marker: Marker) => {
      marker.destroy()
      this.tripMarkers = []
    })
    if (!this.allStops) {
      this.allStops = await AddAllStops(this.map)
      this.AddBusStopEvent(this.allStops)
    }

    if (this.allBLines.length == 0) {
      for (let i = 0; i < lines.length; i++) {
        this.allBLines.push(await InsertPolyline(this.map, lines[i].code, lines[i].color, lines[i].width, lines[i].index))
      }
    }
  }

  async ClearMap() {
    if (this.allBLines.length !== 0) this.allBLines.forEach((line: Polyline) => {
      line.destroy()
      this.allBLines = []
    })
    if (this.tripPolylines.length !== 0) this.tripPolylines.forEach((line: Polyline) => {
      line.destroy()
      this.tripPolylines = []
    })
    if (this.tripMarkers.length !== 0) this.tripMarkers.forEach((marker: Marker) => {
      marker.destroy()
      this.tripMarkers = []
    })
    if (this.allStops !== null) this.allStops.remove()
    this.allStops = null
  }

  async ToggleAllPolyline() {
    if (this.allBLines.length === 0) {
      for (let i = 0; i < lines.length; i++) {
        this.allBLines.push(await InsertPolyline(this.map, lines[i].code, lines[i].color, lines[i].width, lines[i].index))
      }
    } else {
      this.allBLines.forEach((line: Polyline) => {
        line.destroy()
      })
      this.allBLines = []
    }
  }

  async ToggleAllStops() {
    if (this.allStops) {
      this.allStops.remove()
      this.allStops = null
    } else {
      this.allStops = await AddAllStops(this.map)
      this.AddBusStopEvent(this.allStops)
    }
  }

  GeoCode() {
    let options: GeocoderRequest = {
      address: this.searchInput + ' Phnom Penh, Cambodia'
    };
    // Address -> latitude,longitude
    Geocoder.geocode(options)
      .then(async (results: GeocoderResult[]) => {
        if (results[0]) {
          let FullRoute = await Assemble(this.userLocation, results[0].position, this.closeByBusStops)
          return await CreatePath(this.map, FullRoute)
        }
        return 0
      })
  }

  log(obj) {
    console.log(JSON.stringify(obj))
  }

}