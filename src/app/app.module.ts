import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { GoogleMaps } from "@ionic-native/google-maps";
import { HTTP } from '@ionic-native/http';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Geolocation } from '@ionic-native/geolocation';
import { ScreenOrientation } from '@ionic-native/screen-orientation';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ExplorePage } from '../pages/explore/explore';
import { TripChoicesPage } from '../pages/trip-choices/trip-choices';

import { ComponentsModule } from '../components/components.module';
import { DataSharingService } from '../pages/home/services/data-sharing'

@NgModule({
  schemas: [
   CUSTOM_ELEMENTS_SCHEMA
   ],
  declarations: [
    MyApp,
    HomePage,
    TripChoicesPage,
    ExplorePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    ComponentsModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TripChoicesPage,
    ExplorePage,
  ],
  providers: [
    GoogleMaps,
    DataSharingService,
    HTTP,
    LocationAccuracy,
    Geolocation,
    ScreenOrientation,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
