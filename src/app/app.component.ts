import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ScreenOrientation } from '@ionic-native/screen-orientation';

import { HomePage } from '../pages/home/home';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav; 

  rootPage:any;

  pages: Array<{ title: string }>;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    private screenOrientation: ScreenOrientation
    ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.rootPage = HomePage;
      statusBar.overlaysWebView(true);
      statusBar.styleDefault();
      splashScreen.hide();
      this.screenOrientation.lock('portrait');
    });

    this.pages = [
      { title: 'Explore' },
      { title: 'My Trips' },
      { title: 'Static Maps' },
      { title: 'Setting' },
    ];

  }

}
