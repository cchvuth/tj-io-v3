import { Component } from '@angular/core'
import { Events } from 'ionic-angular'
import { DataSharingService } from '../../pages/home/services/data-sharing'

/**
 * Generated class for the BusStopDetailComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'bus-stop-detail',
  templateUrl: 'bus-stop-detail.html'
})
export class BusStopDetailComponent {

  info = {
    id: '',
    line: '',
    stop_number: '',
    location: '',
    summary: ''
  }

  constructor(
    public sharedData:DataSharingService,
    public events: Events
  ) {
    this.sharedData.currentTripDetailChanges.subscribe(info => {
      if (info) this.info = info
    });
  }

  Close() {
    this.events.publish('detail:close');
  }

}
