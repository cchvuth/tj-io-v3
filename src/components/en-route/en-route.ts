import { Component, ChangeDetectorRef } from '@angular/core'
import { Events } from 'ionic-angular'
import { DataSharingService } from '../../pages/home/services/data-sharing'
/**
 * Generated class for the NearestResponseComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'en-route',
  templateUrl: 'en-route.html'
})
export class EnRouteComponent {

  info = {
    id: '',
    line: '',
    stop_number: '',
    summary: '',
    mode: '',
    duration: ''
  }

  constructor(
    public sharedData:DataSharingService,
    private changeDetectorRef: ChangeDetectorRef,
    public events: Events
  ) {
    this.sharedData.currentTripDetailChanges.subscribe(info => {
      this.info = info
    });
  }

  BackToMain() {
    this.events.publish('nav:main');
  }

}
