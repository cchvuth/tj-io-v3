import { Component, ChangeDetectorRef } from '@angular/core'
import { Events } from 'ionic-angular'
import { DataSharingService } from '../../pages/home/services/data-sharing'
/**
 * Generated class for the BusStopIconResponseComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'bus-stop-icon-response',
  templateUrl: 'bus-stop-icon-response.html'
})
export class BusStopIconResponseComponent {

  info = {
    id: '',
    line: '',
    stop_number: '',
    summary: '',
    mode: '',
    duration: ''
  }

  is_viewing_detail: boolean = false

  constructor(
    public sharedData:DataSharingService,
    private changeDetectorRef: ChangeDetectorRef,
    public events: Events
  ) {
    this.sharedData.currentTripDetailChanges.subscribe(info => {
      this.info = info
    });
    events.subscribe('detail:close', () => {
      this.is_viewing_detail = false
      this.changeDetectorRef.detectChanges()
      this.changeDetectorRef.detach()
    })
  }

  Go() {
    this.events.publish('go:marker');
  }

  ViewDetail() {
    this.is_viewing_detail = true
    this.changeDetectorRef.detectChanges()
  }

}
