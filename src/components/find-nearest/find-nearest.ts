import { Component } from '@angular/core';
import { Events } from 'ionic-angular';
/**
 * Generated class for the FindNearestComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'find-nearest',
  templateUrl: 'find-nearest.html'
})
export class FindNearestComponent {
  allStopsToggleStatus:Boolean = true
  allBLinesToggleStatus:Boolean = true
  text: string;

  constructor(public events: Events) {
  }

  GoNearest() {
    this.events.publish('go:nearest');
  }

  ToggleAllStops() {
    this.allStopsToggleStatus ? this.allStopsToggleStatus = false : this.allStopsToggleStatus = true
    this.events.publish('toggle:all-stops');
  }

  ToggleAllBLines() {
    this.allBLinesToggleStatus ? this.allBLinesToggleStatus = false : this.allBLinesToggleStatus = true
    this.events.publish('toggle:all-b-lines');
  }

}
