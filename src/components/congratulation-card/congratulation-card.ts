import { Component } from '@angular/core'
import { Events } from 'ionic-angular'
import { DataSharingService } from '../../pages/home/services/data-sharing'
/**
 * Generated class for the CongratulationCard1Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'congratulation-card',
  templateUrl: 'congratulation-card.html'
})
export class CongratulationCardComponent {

  message = {
    title: '',
    body: ''
  }

  constructor(
    public sharedData:DataSharingService,
    public events: Events
  ) {
    this.sharedData.currentMessageChanges.subscribe(message => {
      if (message) this.message = message
    })
  }

  Close() {
    this.events.publish('alert:close');
  }

}
