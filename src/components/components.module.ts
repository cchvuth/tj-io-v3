import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { CommonModule } from '@angular/common';

import { FindNearestComponent } from './find-nearest/find-nearest';
import { BusStopIconResponseComponent } from './bus-stop-icon-response/bus-stop-icon-response';
import { EnRouteComponent } from './en-route/en-route';
import { BusStopDetailComponent } from './bus-stop-detail/bus-stop-detail';
import { CongratulationCardComponent } from './congratulation-card/congratulation-card';


@NgModule({
	declarations: [
		FindNearestComponent,
	  BusStopIconResponseComponent,
	  EnRouteComponent,
	  BusStopDetailComponent,
	  CongratulationCardComponent
	],
	imports: [IonicModule, CommonModule],
	exports: [
		FindNearestComponent,
    BusStopIconResponseComponent,
    EnRouteComponent,
    BusStopDetailComponent,
    CongratulationCardComponent
  ]
})
export class ComponentsModule {}
